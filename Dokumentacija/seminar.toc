\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}
\contentsline {chapter}{\numberline {2}Osnovni pojmovi}{2}
\contentsline {section}{\numberline {2.1}DeBrujin graf}{2}
\contentsline {section}{\numberline {2.2}Bloom filter}{2}
\contentsline {section}{\numberline {2.3}K-mer}{2}
\contentsline {chapter}{\numberline {3}Reprezentacija grafa sa bloom filterom i cFP skupom}{3}
\contentsline {section}{\numberline {3.1}Izgradnja cFP skupa}{3}
\contentsline {chapter}{\numberline {4}Brojanje K-mera}{5}
\contentsline {section}{\numberline {4.1}Ciklusi }{5}
\contentsline {chapter}{\numberline {5}Obilazak Grafa}{6}
\contentsline {section}{\numberline {5.1}Sa\IeC {\v z}imanje Grafa}{6}
\contentsline {section}{\numberline {5.2}Odabir po\IeC {\v c}etnog K-mera}{6}
\contentsline {section}{\numberline {5.3}Algoritam za obilazak grafa}{7}
\contentsline {section}{\numberline {5.4}Ozna\IeC {\v c}avanje obi\IeC {\dj }enih \IeC {\v c}vorova}{7}
\contentsline {chapter}{\numberline {6}Rezultati}{8}
\contentsline {chapter}{\numberline {7}Pote\IeC {\v s}ko\IeC {\'c}e i mogu\IeC {\'c}a pobolj\IeC {\v s}anja}{9}
\contentsline {chapter}{\numberline {8}Zaklju\IeC {\v c}ak}{10}
\contentsline {chapter}{\numberline {9}Literatura}{11}
\contentsline {chapter}{\numberline {10}Sa\IeC {\v z}etak}{12}

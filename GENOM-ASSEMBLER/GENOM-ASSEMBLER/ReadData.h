#pragma once

#include <string>
#include "Kmer.h"
#include "SequenceData.h"
#include "Kmer.h"

class ReadData
{
  const short rd_sKmerSize = 31;

  SequenceData rd_gdSequence;

  std::string rd_strDescription1;
  std::string rd_strDescription2;
  std::string rd_strQuality;

public:
  ReadData(std::string strDescription1, std::string strSequence, 
    std::string strDescription2, std::string strQuality);
  ReadData(std::string strSequence);
  ~ReadData();

  ullint Size();

  std::string getDescription1();
  std::string getSequence();
  std::string getDescription2();
  std::string getQuality();

  class iterator {
    ullint it_ullIndex;
    const SequenceData& it_sdData;
    
    ullint it_ullRollingHashedKmer;
    ullint it_ullKmerData;
  public:
    iterator(ullint ullIndex, const SequenceData& sdData);
    iterator operator++();
    bool operator != (const iterator& itRhs) const;
    const Kmer operator*() const;
  };

  iterator begin();
  iterator end();
};

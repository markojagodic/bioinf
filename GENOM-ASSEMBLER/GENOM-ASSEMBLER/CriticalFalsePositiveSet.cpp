#include "CriticalFalsePositiveSet.h"

CriticalFalsePositiveSet::CriticalFalsePositiveSet(const std::vector<Kmer>& vS, const BloomFilter& bfFilter, const ullint ulM)
{
  CreateCriticalFalsePositive(vS, bfFilter, ulM);
}

CriticalFalsePositiveSet::~CriticalFalsePositiveSet()
{}

void CriticalFalsePositiveSet::CreateCriticalFalsePositive(const std::vector<Kmer>& vS, const BloomFilter& bfFilter, const ullint ulM)
{
  const char acBases[4] = { 'A', 'C', 'T', 'G' };
  
  // storing all extensions of S for which the Bloom filter answers yes
  for (const Kmer& kmrBase : vS)
  {
    for (const char cBase : acBases) {
      twobit tbExtension = CharToBin(cBase);

      // prefix extension
      Kmer kmrExtensionPrefix = Kmer(tbExtension, kmrBase);
      if (bfFilter.Contains(kmrExtensionPrefix))
      {
        cfp_sCfp.insert(kmrExtensionPrefix);
      }

      // suffix extension
      Kmer kmrExtensionSuffix = Kmer(kmrBase, tbExtension);
      if (bfFilter.Contains(kmrExtensionSuffix))
      {
        cfp_sCfp.insert(kmrExtensionSuffix);
      }
    }
  }

  // TODO: free the bloom filter from memory (store it in disk?!)
  // we skip this step for now

  // fitering cfp set (P in paper)
  std::unordered_set<Kmer> sS = std::unordered_set<Kmer>(vS.begin(), vS.end()); // TODO - binary search for vector<Kmer>

  for (std::unordered_set<Kmer>::iterator it = cfp_sCfp.begin(); it != cfp_sCfp.end();) {
    if (sS.find(*it) != sS.end()) {
      it = cfp_sCfp.erase(it); // c++11: erase() function returns an iterator to next element of the last deleted element
    }
    else {
      ++it;
    }
  }
}

bool CriticalFalsePositiveSet::IsCFP(const Kmer& kmrKmer) const
{
  return cfp_sCfp.find(kmrKmer) != cfp_sCfp.end();
}


ullint CriticalFalsePositiveSet::Size() const
{
  return cfp_sCfp.size();
}


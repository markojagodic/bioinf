#pragma once

#include "DeBrujinGraphComplexKmerNode.h"
#include "BloomFilter.h"
#include "CriticalFalsePositiveSet.h"

#include <string>
#include <vector>

#define DEBRUJIN_NODE const DeBrujinGraphComplexKmerNode&

class KmerTraverser
{
  const BloomFilter& kt_bfAllKmers;
  const CriticalFalsePositiveSet& kt_cfpCriticalKmers;


public:
  KmerTraverser(const BloomFilter& bfAllKmers, const CriticalFalsePositiveSet& cfpCriticalKmers);
  ~KmerTraverser();

  std::string KmerPathToStrig(Kmer& kmrStart, const Direction& dirDirection, ullint ullLength, bool bWithFirstKmer) const;
  std::string PathToString(std::vector<Node*> &vPathNodes) const;
  bool IsTruePositive(const Kmer& kmrKmer) const;
  bool IsSimpleKmer(const Kmer& kmrKmer) const;
  uchar CountNeighbours(const Kmer& kmrKmer) const;
  Direction TraverseFirstNeighbour(Kmer& kmrKmer, Direction& dirPrevDir) const;
  std::vector<Direction> GetNeighbours(const Kmer& kmrBaseKmer) const;
  void DirectTraverse(Kmer& kmrKmer, Direction& dirFromDir, uint& uiLength) const;
  // Direction from kmer A to kmer B.
  Direction GetDirection(const Kmer& kmrKmerA, const Kmer& kmrKmerB) const;
  static Kmer NextKmer(const Kmer& kmrCurrentKmer, const Direction dirNext);
};


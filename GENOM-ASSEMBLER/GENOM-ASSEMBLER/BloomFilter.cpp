#include "BloomFilter.h"
#include "Hash.h"

#include <cstring>

BloomFilter::BloomFilter(uint uiNumberOfHashes) :
  blm_ullintNumberOfHashes(uiNumberOfHashes),
  blm_acFilter(new char[blm_ullintArraySize])
{
  memset(blm_acFilter, 0, blm_ullintArraySize);
}

BloomFilter::~BloomFilter()
{
  delete blm_acFilter;
}

void BloomFilter::Add(const Kmer& kmrKmer)
{
  /* for every hash function stored in vector calculate
  position in bloom filter and set that bit to 1 */
  for (int i = 0; i < blm_ullintNumberOfHashes; i++)
  {
    ullint ullintIndex = Hash::GetHash(kmrKmer, i) % blm_ullintSize;
    ullint ullintArrayIndex = ullintIndex / 8; 
    ullint ullintByteIndex = ullintIndex % 8;

    // set appropriate bit
    char cMask = 1 << ullintByteIndex;
    
    // update appropriate byte
    blm_acFilter[ullintArrayIndex] |= cMask;
  }
}

bool BloomFilter::Contains(const Kmer& kmrKmer) const
{
  for (int i = 0; i < blm_ullintNumberOfHashes; i++)
  {
    ullint ullintIndex = Hash::GetHash(kmrKmer, i) % blm_ullintSize;
    ullint ullintArrayIndex = ullintIndex / 8;
    ullint ullintByteIndex = ullintIndex % 8;

    // set appropriate bit
    char cMask = 1 << ullintByteIndex;

    // update appropriate byte
    if (!(blm_acFilter[ullintArrayIndex] & cMask))
    {
      return false;
    }
  }

  return true;
}

static bool checkOriginalKmers(const std::vector<Kmer>& vKmers, const BloomFilter& bfFilter)
{
  for (const Kmer& kmrKmer : vKmers)
  {
    if (!bfFilter.Contains(kmrKmer)) return false;
  }

  return true;
}

static void checkExtensions(const std::vector<Kmer>& vKmers, const BloomFilter& bfFilter)
{
  //bool test = true;
  //int count1 = 0;
  //int count2 = 0;
  //int vSize = vKmers.size();


  //for (int i = 0; i < vKmers.size(); i++) {
  //  Kmer kKmer = vKmers[i];

  //  char acBases[] = { 'A', 'C', 'T', 'G' };
  //  short count = 0;

  //  for (int j = 0; j < 4; j++) {
  //    twobit tbExtension = CharToBin(acBases[j]);

  //    Kmer kmrNeighbour1 = Kmer(tbExtension, kKmer);
  //    if (bfFilter.contains(kmrNeighbour1)) ++count;

  //    Kmer kmrNeighbour2 = Kmer(kKmer, tbExtension);
  //    if (bfFilter.contains(kmrNeighbour2)) ++count;
  //  }

  //  if (count < 1) {
  //    test = false;
  //  }
  //  else if (count == 1) {
  //    count1++;
  //  }
  //  else if (count == 2) {
  //    count2++;
  //  }
  //  else {
  //    int a = 0;
  //  }
  //}
  //int k = 5;
}

void BloomFilter::AddVector(const std::vector<Kmer>& vKmers)
{
  for (const Kmer& kKmer : vKmers)
  {
    Add(kKmer);
  }

  //bool test = checkOriginalKmers(vKmers, *this);
  //checkExtensions(vKmers, *this);
}

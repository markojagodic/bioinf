#include "SequenceData.h"

#include <math.h>
#include "HelperFunctions.h"
#include "Kmer.h"
#include <cstring>

SequenceData::SequenceData(std::string strData)
{
  if (StringToBinary(strData, sd_pacGenomData, sd_ullByteSize) == true) {
    sd_ullNumElements = strData.size();
  }
  else {
    delete[] sd_pacGenomData;
    sd_ullNumElements = 0;
    sd_ullByteSize = 0;
    sd_pacGenomData = nullptr;
  }
}

static std::string ToStringHelper(ullint ullData) {
  std::string strData;
  for (int i = Kmer::kmr_sKmerSize - 1; i >= 0; i--) {
    strData.push_back(BinToChar((ullData & (0x3ull << (2 * i))) >> (2 * i)));
  }
  return strData;
}

SequenceData::SequenceData(ullint ullData) : SequenceData(ToStringHelper(ullData))
{

}

SequenceData::SequenceData(const SequenceData & sdOriginal)
{
  sd_pacGenomData = new uchar[sdOriginal.sd_ullByteSize];
  memcpy(sd_pacGenomData, sdOriginal.sd_pacGenomData, sdOriginal.sd_ullByteSize);
  sd_ullNumElements = sdOriginal.sd_ullNumElements;
  sd_ullByteSize = sdOriginal.sd_ullByteSize;
}

SequenceData::SequenceData(const twobit cPrefix, const Kmer & kmrOther)
{
  const ullint ullArraySize = kmrOther.kmr_sKmerSize / 4 + (kmrOther.kmr_sKmerSize % 4 == 0 ? 0 : 1);
  sd_pacGenomData = new uchar[ullArraySize];
  memset(sd_pacGenomData, 0, ullArraySize);
  sd_ullByteSize = ullArraySize;
  sd_ullNumElements = kmrOther.kmr_sKmerSize;

  sd_pacGenomData[0] |= cPrefix;
  for (int i = 0; i < kmrOther.kmr_sKmerSize - 1; i++) {
    sd_pacGenomData[(i + 1) / 4] |= kmrOther[i] << (2 * ((i + 1) % 4));
  }
}

SequenceData::SequenceData(const Kmer & kmrOther, const twobit cSuffix)
{
  const ullint ullArraySize = kmrOther.kmr_sKmerSize / 4 + (kmrOther.kmr_sKmerSize % 4 == 0 ? 0 : 1);
  sd_pacGenomData = new uchar[ullArraySize];
  memset(sd_pacGenomData, 0, ullArraySize);
  sd_ullByteSize = ullArraySize;
  sd_ullNumElements = kmrOther.kmr_sKmerSize;

  for (int i = 0; i < kmrOther.kmr_sKmerSize-1; i++) {
    sd_pacGenomData[i / 4] |= kmrOther[i+1] << (2 * (i % 4));
  }
  const int iIndex = kmrOther.kmr_sKmerSize -1;
  sd_pacGenomData[iIndex / 4] |= cSuffix << (2 * (iIndex % 4));
}

SequenceData::~SequenceData()
{
  if (sd_pacGenomData != nullptr) {
    delete[] sd_pacGenomData;
    sd_pacGenomData = nullptr;
  }
}

SequenceData & SequenceData::operator=(const SequenceData & sdOther)
{
  if (sd_ullByteSize != sdOther.sd_ullByteSize) {
    delete sd_pacGenomData;
    sd_pacGenomData = nullptr;
  }
  sd_ullByteSize = sdOther.sd_ullByteSize;
  sd_ullNumElements = sdOther.sd_ullNumElements;

  if (sd_pacGenomData == nullptr) {
    sd_pacGenomData = new uchar[sd_ullByteSize];
  }

  memcpy(sd_pacGenomData, sdOther.sd_pacGenomData, sd_ullByteSize);

  return *this;
}

uchar SequenceData::operator[](ullint ullIndex) const
{
  uchar ucElement = sd_pacGenomData[ullIndex / 4] & (0x3 << (2 * (ullIndex % 4)));
  ucElement >>= 2 * (ullIndex % 4);
  return ucElement;
}

ullint SequenceData::Size() const 
{
  return sd_ullNumElements;
}

std::string SequenceData::ToString()
{
  std::string strRetVal;
  BinaryToString(sd_pacGenomData, sd_ullNumElements, strRetVal);
  return strRetVal;
}

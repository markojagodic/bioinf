#pragma once

#include "Kmer.h"
#include <vector>


class BloomFilter
{
  const ullint blm_ullintNumberOfHashes;
  const ullint blm_ullintArraySize = 1024 * 1024 * 1024; // size of array
  const ullint blm_ullintSize = 8 * blm_ullintArraySize; // number of elements in bloom filter

  char* blm_acFilter;
  
public:
  BloomFilter(uint uiNumberOfHashes = 1);
  ~BloomFilter();

  void Add(const Kmer& kmrKmer);
  bool Contains(const Kmer& kmrKmer) const;
  void AddVector(const std::vector<Kmer>& vKmers);
};

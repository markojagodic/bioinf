#include "DeBrujinGraphComplexKmerNode.h"


DeBrujinGraphComplexKmerNode::DeBrujinGraphComplexKmerNode(const Kmer kmrKmer1, const Kmer kmrKmer2, const Direction dirDir1, const Direction dirDir2, const uint uiLength):
  cno_kmrKmer1(kmrKmer1), cno_kmrKmer2(kmrKmer2), cno_dirDir1(dirDir1), cno_dirDir2(dirDir2), cno_uiLength(uiLength)
{
}

DeBrujinGraphComplexKmerNode::~DeBrujinGraphComplexKmerNode()
{
}

bool DeBrujinGraphComplexKmerNode::operator<(const Node& noNode) const
{
  return false;
}


bool DeBrujinGraphComplexKmerNode::operator==(const DeBrujinGraphComplexKmerNode & cnoOther) const
{
  return cno_kmrKmer1 == cnoOther.cno_kmrKmer1 &&
    cno_kmrKmer2 == cnoOther.cno_kmrKmer2 &&
    cno_dirDir1 == cnoOther.cno_dirDir1 &&
    cno_dirDir2 == cnoOther.cno_dirDir2 &&
    cno_uiLength == cnoOther.cno_uiLength;
}

std::string DeBrujinGraphComplexKmerNode::ToString() const
{
  static std::string astrDirections[] = {
    "DIR__C",
    "DIR_C_",
    "DIR__A",
    "DIR_A_",
    "DIR__T",
    "DIR_T_",
    "DIR__G",
    "DIR_G_",
    "DIR_NONE"
  };

  std::string strRet;
  strRet.append("Kmer1: ");
  strRet.append(cno_kmrKmer1.ToStringNormalized());
  strRet.append(" ---> ");
  strRet.append(astrDirections[cno_dirDir1]);
  strRet.append("\n");
  strRet.append("Kmer2: ");
  strRet.append(cno_kmrKmer2.ToString());
  strRet.append(" ---> ");
  strRet.append(astrDirections[cno_dirDir2]);
  strRet.append("\n");
  strRet.append("Length: ");
  strRet.append(std::to_string(cno_uiLength));

  return strRet;
}

void DeBrujinGraphComplexKmerNode::PrintNode() const
{
  printf("%s\n", ToString().c_str());
}

ullint DeBrujinGraphComplexKmerNode::Hash() const
{
  ullint stHash = Hash::GetHash(cno_kmrKmer1, 0);
  stHash = 100019 * stHash + Hash::GetHash(cno_kmrKmer1, 0);
  stHash = 100007 * stHash + std::hash<int>{}(cno_dirDir1 + 1);
  stHash = 63199831211 * stHash + std::hash<int>{}(cno_dirDir2 + 1);
  stHash = 10037 * stHash + std::hash<uint>{}(cno_uiLength);

  return stHash;
}

ullint DeBrujinGraphComplexKmerNode::Length() const
{
  return cno_uiLength;
}






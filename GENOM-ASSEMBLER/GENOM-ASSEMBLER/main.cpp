#include <cstdio>
#include <iostream>

#include "FastqParser.h"
#include "KmerCountingAlgorithm.h"
#include "BloomFilter.h"
#include "StopWatch.h"
#include "CriticalFalsePositiveSet.h"
#include "BFSGraphTraversal.h"
#include "SpaceEfficientMultiGraphBasedOnBloomFilter.h"
#include "Test.h"
#include "KmerTraverser.h"

int main()
{
  //Test();
  printf("tests passed\n Now loading file started and data processing\n");

  StopWatch swStopWatch;
  std::vector<Kmer> vUniqueKmers;
  Kmer kmrStartingKmer;
  swStopWatch.Start();
  {
    FastqParser fpParser;
    SequencingReads srReads = fpParser.ParseFile(); //read from standard input  
    //SequencingReads srReads = fpParser.ParseFile("ERR2213556.fastq");
    //SequencingReads srReads = fpParser.ParseFile("ERR2225407.fastq");
    //SequencingReads srReads = fpParser.ParseFile("SRR6402095.fastq");
    //SequencingReads srReads = fpParser.ParseFile("simpler.txt");
    //SequencingReads srReads = fpParser.ParseFile("simple.txt");
    //SequencingReads srReads = fpParser.ParseFile("SRR1284936.fastq");
    //SequencingReads srReads = fpParser.ParseFile("e1.fastq");
    if (srReads.size() == 0) {
      printf("no input file added\n");
    }
    swStopWatch.TimeStamp();
    KmerCountingAlgorithm kcaCounter = KmerCountingAlgorithm(srReads);
    vUniqueKmers = kcaCounter.Count(kmrStartingKmer, 5);
    printf("number of unique kmers = %lld\n", vUniqueKmers.size());
  }
  swStopWatch.TimeStamp();
  BloomFilter bfFilter = BloomFilter(3);
  bfFilter.AddVector(vUniqueKmers);
  swStopWatch.TimeStamp();
  CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vUniqueKmers, bfFilter, 5);
  printf("CFP size = %llu\n", cfpSet.Size());
  swStopWatch.TimeStamp();


  KmerTraverser ktTraverser(bfFilter, cfpSet);
  SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(kmrStartingKmer, ktTraverser);
  BFSGraphTraversal bfsTraversal;
  printf("started traversing graph\n");
  std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);
  swStopWatch.TimeStamp();

  std::string strPath = ktTraverser.PathToString(aNodesPath);
  //PrintAllFourVersions(strPath);

  printf("output: %s\n", strPath.c_str());

  //assert(CompareAllCombosSequences(strPath, "ATACAAATGCCCCCCACTGTCCCTATTTATCATTATGTCGAGTCCCGAAAACCAACAAAATAGGATCGACATCCTATTCTATTATTCCATGCACGAATCTTCGTCATACTGTATTCGGATCGATATATTTGGGATCGTATATTAGCG"));

  //long count = 0;
  //long countBloomYes = 0;
  //for (const Kmer& kmrBase : vUniqueKmers) {
  //  char acBases[] = { 'A', 'C', 'T', 'G' };

  //  for (int i = 0; i < 4; i++) {
  //    twobit tbExtension = CharToBin(acBases[i]);
  //    Kmer kmrNeighbour = Kmer(tbExtension, kmrBase);

  //    if (bfFilter.contains(kmrNeighbour)) ++countBloomYes; 

  //    Kmer kmrNeighbour2 = Kmer(kmrBase, tbExtension);

  //    if (bfFilter.contains(kmrNeighbour2)) ++countBloomYes;
  //  }

  //  //if (bfFilter.contains(kmrBase)) countBloomYes++;
  //}

  //printf("Bloom yes = %d\n", countBloomYes);



  //parser.parseFile("ERR2225407.fastq");
  //parser.parseFile("test.txt");

  //std::cout << srReads[0].getSequence() << std::endl;
  //Kmer kmrKmer = srReads[0][1];
  //std::cout << kmrKmer << std::endl;
  //
  //KmerCountingAlgorithm kcaCounter(srReads);
  //swStopWatch.Start();
  //std::vector<Kmer> vBinKmer =  kcaCounter.Count(3);
  //printf("Number of kmers = %d\n", vBinKmer.size());
  //swStopWatch.End();
  //SequencingReads srReads = fpParser.parseFile("simple.txt");
  //Kmer kmrKmer = srReads[0][1];
  //std::cout << kmrKmer << std::endl;
  //
  //BloomFilter filter = BloomFilter();

  swStopWatch.End();
  printf("Finished\n");
  //system("PAUSE");
  return 0;
}
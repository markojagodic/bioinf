#pragma once

#include <vector>
#include "ReadData.h"

class SequencingReads
{
  std::vector<ReadData> sr_vAllReads;

public:
  SequencingReads();
  ~SequencingReads();
  void addRead(std::string& a, std::string& b, std::string& c, std::string&d);
  ullint size() const;
  void printReads();
  ReadData& operator[] (int i);
};

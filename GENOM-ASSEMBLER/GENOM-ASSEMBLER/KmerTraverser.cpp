#include "KmerTraverser.h"
#include "iostream"
#include "Kmer.h"

KmerTraverser::KmerTraverser(const BloomFilter& bfAllKmers, const CriticalFalsePositiveSet& cfpCriticalKmers):
  kt_bfAllKmers(bfAllKmers), kt_cfpCriticalKmers(cfpCriticalKmers)
{
}

KmerTraverser::~KmerTraverser()
{
}

static twobit DirectionToTwobit(const Direction dirDirection) {
  return (twobit)(dirDirection / 2);
}

static bool NodeContainKmer(const DeBrujinGraphComplexKmerNode& nNode, const Kmer& kmrKmer) {
  bool bRet = nNode.cno_kmrKmer1 == kmrKmer || nNode.cno_kmrKmer2 == kmrKmer;
  return bRet;
}

// returns true if a given kmer is a first kmer in a given DeBrujinNode 
static bool IsFirstKmer(const Kmer& kmrKmer, const DeBrujinGraphComplexKmerNode& cnoNode) {
  bool bRet = kmrKmer == cnoNode.cno_kmrKmer1;
  return bRet;
}

std::string KmerTraverser::KmerPathToStrig(Kmer& kmrCurrent, const Direction & dirDirection, ullint ullLength, bool bWithFirstKmer) const
{
  if (ullLength == 0 && bWithFirstKmer) {
    return kmrCurrent.ToString();
  }

  std::string strRet;
  strRet.reserve(ullLength + bWithFirstKmer ? Kmer::kmr_sKmerSize : 0);
  Kmer kmrNextNeighbour = NextKmer(kmrCurrent, dirDirection);
  Direction dirPrevDir = GetDirection(kmrNextNeighbour, kmrCurrent);
  --ullLength;

  if (bWithFirstKmer) {
    std::string strStartKmer = kmrCurrent.ToStringNormalized();
    if (dirDirection % 2) {
      std::reverse(strStartKmer.begin(), strStartKmer.end());
    }

    strRet.append(strStartKmer);
  }

  // use kmrCurrent to travese throught kmer path
  kmrCurrent = kmrNextNeighbour;

  // starting direction nucleotide (because TraverseFirst starts from first neighbour)
  twobit tbDirection = DirectionToTwobit(dirDirection);
  char cNucleotide = BinToChar(tbDirection);
  strRet.push_back(cNucleotide);

  while(ullLength-- > 0) {
    Direction dirNextDir = TraverseFirstNeighbour(kmrCurrent, dirPrevDir);
    twobit tbNextDir = DirectionToTwobit(dirNextDir);
    char cExtension = BinToChar(tbNextDir);
    strRet.push_back(cExtension);
  }

  return strRet;
}

static ullint CalculatePathLength(const std::vector<Node*> vPathNodes) {
  if (vPathNodes.size() == 0) return 0;

  ullint ullLength = Kmer::kmr_sKmerSize;
  for (const Node* nNode : vPathNodes) {
    ullLength += nNode->Length();
  }

  return ullLength;
}

std::string KmerTraverser::PathToString(std::vector<Node*> &vPathNodes) const
{
  std::string strPath;

  if (vPathNodes.size() == 0) {
    return strPath;
  }

  ullint ullPathLength = CalculatePathLength(vPathNodes);
  strPath.reserve(ullPathLength);

  DeBrujinGraphComplexKmerNode cnoFirstNode = (DEBRUJIN_NODE)(*vPathNodes[0]);
  if (vPathNodes.size() == 1) {
    Kmer kmrKmerTmp = cnoFirstNode.cno_kmrKmer1;
    strPath.append(KmerPathToStrig(kmrKmerTmp, cnoFirstNode.cno_dirDir1, cnoFirstNode.cno_uiLength, true));
    return strPath;
  }

  // now we are shure that there are 2 or more nodes that we need connect

  // define starting kmer from first node
  Kmer kmrCurrentKmer = cnoFirstNode.cno_kmrKmer1;
  Direction dirCurrentDiretion = cnoFirstNode.cno_dirDir1;
  ullint ullLength = cnoFirstNode.cno_uiLength;

  DeBrujinGraphComplexKmerNode cnoSecondNode = (DEBRUJIN_NODE)(*vPathNodes[1]);
  if (NodeContainKmer(cnoSecondNode, kmrCurrentKmer)) {
    kmrCurrentKmer = cnoFirstNode.cno_kmrKmer2;
    dirCurrentDiretion = cnoFirstNode.cno_dirDir2;
  }
  assert(!NodeContainKmer(cnoSecondNode, kmrCurrentKmer));
  // add first node and update kmrCurrentKmer to next kmer (by using KmerPathToString)
  strPath.append(KmerPathToStrig(kmrCurrentKmer, dirCurrentDiretion, ullLength, true));

  // add all other nucleotides on path
  for (ullint i = 1; i < vPathNodes.size(); i++) {
    DeBrujinGraphComplexKmerNode cnoDeBrujinNode = (DEBRUJIN_NODE)(*vPathNodes[i]);

    bool bNotFailed = NodeContainKmer(cnoDeBrujinNode, kmrCurrentKmer);
    if (!bNotFailed) {
      for (Node* pnoNodes : vPathNodes) {
        pnoNodes->PrintNode();
      }
      printf("kmrCurrentKmer: %s\n node: %s\n", kmrCurrentKmer.ToStringNormalized().c_str(), cnoDeBrujinNode.ToString().c_str());
    }
    assert(bNotFailed);

    // first update kmer, direction, and length for next node
    if (IsFirstKmer(kmrCurrentKmer, cnoDeBrujinNode)) {
      //kmrCurrentKmer = cnoDeBrujinNode.cno_kmrKmer1;
      dirCurrentDiretion = cnoDeBrujinNode.cno_dirDir1;
      ullLength = cnoDeBrujinNode.cno_uiLength;
    } 
    else {
      //kmrCurrentKmer = cnoDeBrujinNode.cno_kmrKmer2;
      dirCurrentDiretion = cnoDeBrujinNode.cno_dirDir2;
      ullLength = cnoDeBrujinNode.cno_uiLength;
    }

    strPath.append(KmerPathToStrig(kmrCurrentKmer, dirCurrentDiretion, ullLength, false));
  }

  return strPath;
}

bool KmerTraverser::IsTruePositive(const Kmer & kmrKmer) const
{
  return kt_bfAllKmers.Contains(kmrKmer) && !kt_cfpCriticalKmers.IsCFP(kmrKmer);
}

bool KmerTraverser::IsSimpleKmer(const Kmer & kmrKmer) const
{
  ullint ullNoNeighbours = CountNeighbours(kmrKmer);
  return ullNoNeighbours == 2;
}


uchar KmerTraverser::CountNeighbours(const Kmer& kmrKmer) const
{
  uchar count = 0;

  for (twobit i = 0; i < 8; ++i) {
    twobit tbDir = i / 2;
    Kmer kmrNeighbour = i % 2 ? Kmer(tbDir, kmrKmer) : Kmer(kmrKmer, tbDir);
    if (IsTruePositive(kmrNeighbour)) {
      ++count;
    }
  }

  return count;
}

Direction KmerTraverser::TraverseFirstNeighbour(Kmer& kmrKmer, Direction& dirPrevDir) const
{
  for (twobit i = 0; i < 8; ++i) {
    Direction dirCurrent = (Direction)i;
    if (dirPrevDir == dirCurrent) continue;

    twobit tbDir = i / 2;
    Kmer kmrNeighbour = i % 2 ? Kmer(tbDir, kmrKmer) : Kmer(kmrKmer, tbDir);

    if (IsTruePositive(kmrNeighbour)) {
      dirPrevDir = GetDirection(kmrNeighbour, kmrKmer);
      kmrKmer = kmrNeighbour;
      return (Direction)i;
    }
  }

  assert(false);
  return DIR_NONE;
}

std::vector<Direction> KmerTraverser::GetNeighbours(const Kmer & kmrBaseKmer) const
{
  std::vector<Direction> vRetNodes;

  for (twobit i = 0; i < 8; ++i) {
    twobit tbDir = i / 2;
    Kmer kmrNeighbour = i % 2 ? Kmer(tbDir, kmrBaseKmer) : Kmer(kmrBaseKmer, tbDir);

    if (IsTruePositive(kmrNeighbour)) {
      vRetNodes.push_back(GetDirection(kmrBaseKmer, kmrNeighbour));
    }
  }

  return vRetNodes;
}

// 
void KmerTraverser::DirectTraverse(Kmer& kmrKmer, Direction& dirFromDir, uint& uiLength) const
{
  Kmer kmrPrevKmer;
  const Kmer kmrStartKmer = KmerTraverser::NextKmer(kmrKmer, dirFromDir);

  while (IsSimpleKmer(kmrKmer)) {
    kmrPrevKmer = kmrKmer;
    TraverseFirstNeighbour(kmrKmer, dirFromDir);

    if (kmrKmer == kmrPrevKmer || kmrStartKmer == kmrKmer) {
      //printf("%d %d\n", kmrKmer == kmrPrevKmer, kmrStartKmer == kmrKmer);
      uiLength += kmrKmer == kmrPrevKmer;
      break;
    }

    ++uiLength;
  }
}

Direction KmerTraverser::GetDirection(const Kmer & kmrKmerA, const Kmer & kmrKmerB) const
{
  for (twobit i = 0; i < 8; ++i) {
    twobit tbDir = i / 2;
    Kmer kmrNeighbour = i % 2 ? Kmer(tbDir, kmrKmerA) : Kmer(kmrKmerA, tbDir);

    if (kmrNeighbour == kmrKmerB) {
      return (Direction)i;
    }
  }

  PrintAllFourVersions(kmrKmerA.ToString());
  PrintAllFourVersions(kmrKmerB.ToString());

  assert(false);
  return DIR_NONE;
}

Kmer KmerTraverser::NextKmer(const Kmer & kmrCurrentKmer, const Direction dirNext)
{
  twobit tbDir = DirectionToTwobit(dirNext);
  Kmer kmrNeighbour = dirNext % 2 ? Kmer(tbDir, kmrCurrentKmer) : Kmer(kmrCurrentKmer, tbDir);
  return kmrNeighbour;
}

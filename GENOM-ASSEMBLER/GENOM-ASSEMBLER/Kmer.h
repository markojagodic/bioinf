#pragma once

#include <string>
#include "SequenceData.h"

class SequenceData;

enum InternalState {
  IS_NORMAL_NORMAL = 0,
  IS_NORMAL_REVERSED,
  IS_INVERTED_NORMAL,
  IS_INVERTED_REVERSED
};

/* this class will contain one k-mer 
see https://en.wikipedia.org/wiki/K-mer
*/
class Kmer
{
  binKmer kmr_bkKmerBinaryRepresentation;
  InternalState kmr_isInternalState;
public:
  static const short kmr_sKmerSize = 31;
  Kmer();
  Kmer(const std::string &strData);
  Kmer(const twobit cPrefix, const Kmer& kmrOther);
  Kmer(const Kmer& kmrOther, const twobit cSuffix);
  Kmer(const Kmer& kmrOther);
  Kmer(const ullint ullKmerBits);
  ~Kmer();

  int Size() const;
  std::string ToString() const;
  std::string ToStringNormalized() const;
  ullint ToUllint() const;

  Kmer& operator = (const Kmer &kmrOther);
  bool operator < (const Kmer &kmrOther) const;
  twobit operator [] (const int iIndex) const;
  bool operator == (const Kmer &kmrOther) const;
  bool operator != (const Kmer &kmrOther) const;
  friend std::ostream& operator<<(std::ostream& ostStream, const Kmer& kmrKmer);

private:
  binKmer CacheTrueKmerValue();
  void AddPrefix(const twobit cPrefix, const Kmer& kmrOther);
  void AddSuffix(const Kmer& kmrOther, const twobit cSuffix);
};

namespace std
{
  template<>
  struct hash<Kmer>
  {
    ullint operator()(const Kmer& kmrKmer) const
    {
      return std::hash<unsigned long long>()((unsigned long long)kmrKmer.ToUllint());
    }
  };
}
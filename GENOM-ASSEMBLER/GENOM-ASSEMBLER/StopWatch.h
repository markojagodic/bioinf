#pragma once

#include <chrono>

class StopWatch
{
  std::chrono::time_point<std::chrono::steady_clock> sw_tpBeginning;
  std::chrono::time_point<std::chrono::steady_clock> sw_tpTimeStamp;

public:
  StopWatch();
  ~StopWatch();

  void Start();
  void TimeStamp();
  void End();
};


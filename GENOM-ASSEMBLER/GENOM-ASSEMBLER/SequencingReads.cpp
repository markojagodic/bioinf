#include "SequencingReads.h"
#include <iostream>
#include <string>

SequencingReads::SequencingReads()
{
}

SequencingReads::~SequencingReads()
{
}

void SequencingReads::addRead(std::string& strDescription1, std::string& strSequence, std::string& strDescription2, std::string& strQuality)
{
  //sr_vAllReads.push_back(ReadData(strDescription1, strSequence, strDescription2, strQuality));
  sr_vAllReads.push_back(ReadData("", strSequence, "", ""));
}

ullint SequencingReads::size() const
{
  return sr_vAllReads.size();
}

ReadData& SequencingReads::operator[](int i)
{
  return sr_vAllReads[i]; 
}

void SequencingReads::printReads()
{
  for (int i = 0; i < sr_vAllReads.size(); i++)
  {
    std::cout << std::endl;
    std::cout << "desctiption1: " << sr_vAllReads[i].getDescription1() << std::endl;
    std::cout << "sequence: " << sr_vAllReads[i].getSequence() << std::endl;
    std::cout << "desctiption2: " << sr_vAllReads[i].getDescription2() << std::endl;
    std::cout << "quality: " << sr_vAllReads[i].getQuality() << std::endl;
    std::cout << " ------------------------------------------------------------ " << std::endl;
  }
}



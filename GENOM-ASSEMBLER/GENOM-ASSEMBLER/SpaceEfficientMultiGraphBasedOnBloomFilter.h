#pragma once

#include "Graph.h"
#include "KmerTraverser.h"
#include "DeBrujinGraphComplexKmerNode.h"

#include <unordered_set>
#include <string>


struct pairhash {
public:
  template <typename T, typename U>
  std::size_t operator()(const std::pair<T, U> &x) const
  {
    return std::hash<T>()(x.first) ^ (std::hash<U>()(x.second) << 1);
  }
};


class SpaceEfficientMultiGraphBasedOnBloomFilter :
  public MultiGraph
{
  const Kmer& mgr_kmrStartingKmer;
  const KmerTraverser& mgr_ktTreverser;

  std::unordered_set<std::pair<Kmer, Direction>, pairhash> mgr_sVisited;
  std::vector<Node*> mgr_vCreatedNodes;

  Node* CreateNewNode(const Kmer kmrKmer1, const Kmer kmrKmer2, const Direction dirDir1, const Direction dirDir2, const uint uiLength);
  Node* CreateNewNode(const Kmer& kmrCurrentKmer);

  bool IsVisited(const Kmer& kmrKmer, const Direction dirDirection) const;
  void GetNeighboursForKmer(const Kmer& kmrBaseKmer, const Direction& dirDontExpand, std::vector<Node*>& vOtherKmerNeighbours);
public:
  SpaceEfficientMultiGraphBasedOnBloomFilter(const Kmer& kmrRandomStartingKmer, const KmerTraverser& ktTraverser);
  virtual ~SpaceEfficientMultiGraphBasedOnBloomFilter();

  virtual std::vector<Node*> GetNeighbors(const Node& noNode) override;
  virtual void MarkVisited(const Node & noNode, bool bVisited) override;
  virtual bool IsVisited(const Node & noNode) const override;
  virtual Node * GetStartingNotVisitedNode();
};



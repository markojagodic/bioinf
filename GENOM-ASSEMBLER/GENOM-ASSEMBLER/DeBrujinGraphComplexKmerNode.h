#pragma once

#include <string>

#include "Graph.h"
#include "Kmer.h"
#include "Hash.h"
#include "BloomFilter.h"


enum Direction {
  DIR__C = 0,
  DIR_C_, // 1
  DIR__A, // 2
  DIR_A_, // 3
  DIR__T, // 4
  DIR_T_, // 5
  DIR__G, // 6
  DIR_G_, // 7
  DIR_NONE //8
};


class DeBrujinGraphComplexKmerNode :
  public Node
{
public:
  const Kmer cno_kmrKmer1;
  const Kmer cno_kmrKmer2;
  const Direction cno_dirDir1;
  const Direction cno_dirDir2;
  const uint cno_uiLength;

  //DeBrujinGraphComplexKmerNode(const DeBrujinGraphComplexKmerNode& cnoOther);
  DeBrujinGraphComplexKmerNode(const Kmer kmrKmer1, const Kmer kmrKmer2, const Direction dirDir1, const Direction dirDir2, const uint uiLength);
  virtual ~DeBrujinGraphComplexKmerNode();
  virtual bool operator<(const Node& noNode) const;
  bool operator==(const DeBrujinGraphComplexKmerNode& cnoOther) const;
  virtual std::string ToString() const;
  virtual void PrintNode() const;
  virtual ullint Hash() const;
  virtual ullint Length() const;
};


namespace std
{
  template<>
  struct hash<DeBrujinGraphComplexKmerNode>
  {
    ullint operator()(const DeBrujinGraphComplexKmerNode& cnoNode) const
    {
      return cnoNode.Hash();
    }
  };
}

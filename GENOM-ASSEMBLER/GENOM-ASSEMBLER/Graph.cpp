#include "Graph.h"



MultiGraph::MultiGraph()
{
}


MultiGraph::~MultiGraph()
{
}

Node::Node()
{
}

Node::~Node()
{
}

bool Node::operator==(const Node & noOther) const
{
  const Node &noCurrent = *this;
  return !(noCurrent != noOther);
}

bool Node::operator!=(const Node & noOther) const
{
  const Node &noCurrent = *this;
  return noCurrent > noOther || noCurrent < noOther;
}

bool Node::operator > (const Node & noOther) const
{
  const Node &noCurrent = *this;
  return noOther < noCurrent;
}

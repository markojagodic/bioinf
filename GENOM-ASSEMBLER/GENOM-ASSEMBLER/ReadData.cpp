#include "ReadData.h"
#include <string>
#include <iostream>

ReadData::ReadData(std::string strDescription1, std::string strSequence,
  std::string strDescription2, std::string strQuality) :
  rd_strDescription1(strDescription1), rd_gdSequence(strSequence), rd_strDescription2(strDescription2), rd_strQuality(strQuality)
{}

ReadData::ReadData(std::string strSequence):
  rd_strDescription1(""), rd_gdSequence(strSequence), rd_strDescription2(""), rd_strQuality("")
{
}

ReadData::~ReadData()
{
}

ullint ReadData::Size()
{
  return rd_gdSequence.Size() - rd_sKmerSize + 1;
}

std::string ReadData::getDescription1()
{
  return rd_strDescription1;
}

std::string ReadData::getSequence()
{
  return rd_gdSequence.ToString();
}

std::string ReadData::getDescription2()
{
  return rd_strDescription2;
}

std::string ReadData::getQuality()
{
  return rd_strQuality;
}

ReadData::iterator ReadData::begin()
{
  if(rd_gdSequence.Size() == 0){
    return ReadData::iterator(0, 0);
  }
  else {
    return ReadData::iterator(0, rd_gdSequence);
  }
}

ReadData::iterator ReadData::end()
{
  if (rd_gdSequence.Size() == 0) {
    return ReadData::iterator(0, 0);
  }
  else {
    return ReadData::iterator(Size(), rd_gdSequence);
  }
}

ReadData::iterator::iterator(ullint ullIndex, const SequenceData & sdData) : it_sdData(sdData)
{

  if (&it_sdData == nullptr) return;

  it_ullIndex = ullIndex;

  it_ullKmerData = 0;
  for (int i = 0; i < Kmer::kmr_sKmerSize; i++) {
    it_ullKmerData <<= 2;
    it_ullKmerData |= it_sdData[i + ullIndex];
  }
  for (int i = Kmer::kmr_sKmerSize; i < 32; i++) {
    it_ullKmerData &= ~((0x3ll) << (i * 2));
  }
}

ReadData::iterator ReadData::iterator::operator++()
{
  it_ullIndex++;
  it_ullKmerData <<= 2;
  it_ullKmerData |= it_sdData[it_ullIndex + Kmer::kmr_sKmerSize - 1];
  
  for (int i = Kmer::kmr_sKmerSize; i < 32; i++) {
    it_ullKmerData &= ~((0x3ull) << (i * 2));
  }
  return *this;
}

bool ReadData::iterator::operator!=(const iterator & itRhs) const
{
  return it_ullIndex != itRhs.it_ullIndex;
}

const Kmer ReadData::iterator::operator*() const
{
  return Kmer(it_ullKmerData);
}

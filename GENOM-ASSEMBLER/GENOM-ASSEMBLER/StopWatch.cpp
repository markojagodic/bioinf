#include "StopWatch.h"

#include <cstdio>
#include <chrono>

StopWatch::StopWatch()
{
}


StopWatch::~StopWatch()
{
}

void StopWatch::Start()
{
#ifdef CHRONO_ENABLED
  sw_tpTimeStamp = sw_tpBeginning = std::chrono::high_resolution_clock::now();
#endif // !
}

void StopWatch::End()
{
#ifdef CHRONO_ENABLED
  auto tmEnding = std::chrono::high_resolution_clock::now();
  printf("elipsed time in seconds %lf\n", std::chrono::duration_cast<std::chrono::nanoseconds>(tmEnding - sw_tpBeginning).count() / (1e9));
#endif
}

void StopWatch::TimeStamp()
{
#ifdef CHRONO_ENABLED
  auto tmCurrent = std::chrono::high_resolution_clock::now();
  printf("Time passed from last time stamp:  %lf\n", std::chrono::duration_cast<std::chrono::nanoseconds>(tmCurrent - sw_tpTimeStamp).count() / (1e9));
  sw_tpTimeStamp = tmCurrent;
#endif
}

#pragma once

#include "Kmer.h"
#include "BloomFilter.h"
#include <unordered_set>
#include <vector>

class CriticalFalsePositiveSet
{
  std::unordered_set<Kmer> cfp_sCfp;

  void CreateCriticalFalsePositive(const std::vector<Kmer>& vS, const BloomFilter& bfFilter, const ullint ulM);
public:
  CriticalFalsePositiveSet(const std::vector<Kmer>& vS, const BloomFilter& bfFilter, const ullint ulM);
  ~CriticalFalsePositiveSet();
  bool IsCFP(const Kmer& kmrKmer) const;
  ullint Size() const;
};

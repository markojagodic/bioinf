#pragma once

#include <assert.h>
#include <string>
#include <algorithm>

typedef unsigned int uint;
typedef unsigned long long ullint;
typedef unsigned char uchar;

typedef uchar twobit;
typedef ullint binKmer;

twobit CharToBin(char cIn);
char BinToChar(twobit ucBin);

bool StringToBinary(const std::string strData, uchar *&pacBinaryData, ullint &ullDataSize);
void BinaryToString(const uchar * pacBinaryData, const ullint &ullDataSize, std::string &strData);

twobit InvertSequenceChar(const twobit tbInput);

std::string InvertSequence(const std::string strOriginal);
void PrintAllFourVersions(const std::string strSequence);

bool CompareAllCombosSequences(const std::string strS1, const std::string strS2);

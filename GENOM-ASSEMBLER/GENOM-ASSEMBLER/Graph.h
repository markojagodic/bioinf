#pragma once

#include <vector>
#include "HelperFunctions.h"


//Node will be only complex node like in paper
class Node {
public:
  Node();
  virtual ~Node();

  virtual bool operator < (const Node& noOther) const = 0;
  bool operator == (const Node& noOther) const;
  bool operator != (const Node& noOther) const;
  bool operator > (const Node& noOther) const;
  virtual ullint Length() const = 0;
  virtual std::string ToString() const = 0;
  virtual void PrintNode() const = 0;
  virtual ullint Hash() const = 0;
};


namespace std
{
  template<>
  struct hash<Node*>
  {
    ullint operator()(const Node* cnoNode) const
    {
      return cnoNode->Hash();
    }
  };
}

class MultiGraph
{
public:
  MultiGraph();
  virtual ~MultiGraph();

  virtual std::vector<Node*> GetNeighbors(const Node &noCurrent) = 0;
  virtual void MarkVisited(const Node &noNode, bool bVisited) = 0;
  virtual bool IsVisited(const Node &noNode) const = 0;

  virtual Node* GetStartingNotVisitedNode() = 0;
};


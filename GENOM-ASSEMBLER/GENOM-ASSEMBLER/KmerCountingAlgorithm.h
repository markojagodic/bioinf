#pragma once

#include "SequencingReads.h"
#include "Trie.h"

class KmerCountingAlgorithm
{
  SequencingReads *kca_psrReads;
public:
  KmerCountingAlgorithm() = default;
  KmerCountingAlgorithm(SequencingReads &sr);
  std::vector<Kmer> Count(Kmer &kmrStartingKmer, unsigned short usMinRepetition = 10);
  virtual ~KmerCountingAlgorithm();
};


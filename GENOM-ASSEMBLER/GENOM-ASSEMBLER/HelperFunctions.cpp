#include "HelperFunctions.h"

#include <cstring>
#include <cstdio>

// ~G = C
// ~T = A

twobit CharToBin(char cIn)
{
  switch (cIn)
  {
  case 'A':
    return 1;
  case 'C':
    return 0;
  case 'T':
    return 2;
  case 'G':
    return 3;
  default:
    return 'E';
  }
}

char BinToChar(twobit ucBin) {
  switch (ucBin)
  {
  case 1:
    return 'A';
  case 0:
    return 'C';
  case 2:
    return 'T';
  case 3:
    return 'G';
  default:
    assert(false);
    return 'E';
  }
}

bool StringToBinary(const std::string strData, uchar *&pacBinaryData, ullint &ullDataSize)
{
  ullDataSize = strData.size() / 4 + (strData.size() % 4 ? 1 : 0);
  pacBinaryData = new uchar[ullDataSize];
  memset(pacBinaryData, 0, ullDataSize);

  ullint ullCounter = 0;
  for (char c : strData) {
    uchar ucBinChar = CharToBin(c);
    if (ucBinChar == 'E') {
      return false;
    }
    pacBinaryData[ullCounter / 4] |= ucBinChar << (2 * (ullCounter % 4));
    ullCounter++;
  }
  return true;
}

void BinaryToString(const uchar * pacBinaryData, const ullint & ullDataSize, std::string & strData)
{
  strData.clear();
  for (int i = 0; i < ullDataSize; i++) {
    strData.push_back(BinToChar((pacBinaryData[i / 4] & 0x3 << (2 * (i % 4))) >> (2 * (i % 4))));
  }
}

twobit InvertSequenceChar(const twobit tbInput)
{
  return (~tbInput) & 0x3;
}

std::string InvertSequence(const std::string strOriginal)
{
  std::string strRet;
  for (auto elem : strOriginal) {
    strRet.push_back(BinToChar((~CharToBin(elem)) & 0x3));
  }

  return strRet;
}

void PrintAllFourVersions(const std::string strSequence)
{
  std::string strIndent = "    ";

  std::string strReverse(strSequence);
  std::reverse(strReverse.begin(), strReverse.end());

  printf("------------------------------------------------------\n");
  printf("%s%s\n", strIndent.c_str(), strSequence.c_str());
  printf("%s%s\n", strIndent.c_str(), InvertSequence(strSequence).c_str());
  printf("%s%s\n", strIndent.c_str(), strReverse.c_str());
  printf("%s%s\n", strIndent.c_str(), InvertSequence(strReverse).c_str());
  printf("------------------------------------------------------\n");
}

bool CompareAllCombosSequences(const std::string strS1, const std::string strS2)
{
  std::string strTmp = strS1;
  if (strTmp == strS2) {
    return true;
  }

  std::reverse(strTmp.begin(), strTmp.end());
  if (strTmp == strS2) {
    return true;
  }

  strTmp = InvertSequence(strTmp);
  if (strTmp == strS2) {
    return true;
  }

  std::reverse(strTmp.begin(), strTmp.end());
  if (strTmp == strS2) {
    return true;
  }

  return false;
}



#include "KmerCountingAlgorithm.h"

#include <map>
#include <unordered_map>

KmerCountingAlgorithm::KmerCountingAlgorithm(SequencingReads &sr) : kca_psrReads(&sr)
{
}

std::vector<Kmer> KmerCountingAlgorithm::Count(Kmer &kmrStartingKmer, unsigned short usMinRepetition /* = 10*/)
{
  SequencingReads& srReads = *kca_psrReads;

  int iMaxKmerFrequency = 0;

  std::vector<Kmer> vRetVal;
  {
    std::unordered_map<Kmer, int> mCount;
    
    for (int i = 0; i < srReads.size(); i++) {

      if (100 * i / srReads.size() != 100 * (i - 1) / srReads.size()) {
        printf("Counting progress: %llu%%\n", 100 * i / srReads.size());
      }

      ReadData& rdRead = srReads[i];
      for (const Kmer& kmrKmer : rdRead) {
        int iFrequency = ++mCount[kmrKmer];
        if (iMaxKmerFrequency < iFrequency) {
          iFrequency = iMaxKmerFrequency;
          kmrStartingKmer = kmrKmer;
        }
      }
    }

    printf("counted elements %llu\n", mCount.size());

    vRetVal.reserve(mCount.size());
    int iProgress = 0;
    for (auto pairElem : mCount) {
      iProgress++;
      if (100 * iProgress / mCount.size() != 100 * (iProgress - 1) / mCount.size()) {
        printf("Filtering progress: %llu%%\n", 100 * iProgress / mCount.size());
      }
      if (pairElem.second > usMinRepetition) {
        vRetVal.push_back(pairElem.first);
      }
    }
  }

  return vRetVal;
}


KmerCountingAlgorithm::~KmerCountingAlgorithm()
{
}

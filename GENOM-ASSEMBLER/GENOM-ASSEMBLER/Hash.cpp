#include "Hash.h"

#ifndef HASH_CPP
#define HASH_CPP

ullint Hash::GetHash(const Kmer& kmrKmer, int iVersion) 
{
  ullint stHash = 0x01307BABA;

  ullint a = h_apCoefficients[iVersion].first;
  ullint b = h_apCoefficients[iVersion].second;

  for (int i = 0; i < Kmer::kmr_sKmerSize; i++)
  {
    stHash = a * stHash + (kmrKmer[i] + 1) * b;
  }

  return stHash;
}

const std::array<std::pair<ullint, ullint>, 9> Hash::h_apCoefficients = {
  std::make_pair(10000019, 133300033),
  std::make_pair(533030019, 1533030041),
  std::make_pair(153300023, 453300041),
  std::make_pair(4533015989, 10093),
  std::make_pair(93122121233, 1097),
  std::make_pair(93436362703, 197),
  std::make_pair(7347159713, 1033),
  std::make_pair(931212331219, 10039),
  std::make_pair(63199831211, 100007),
};


#endif
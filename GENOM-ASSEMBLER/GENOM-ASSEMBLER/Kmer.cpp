#include "Kmer.h"
#include "HelperFunctions.h"
#include "Hash.h"

#include <iostream>

Kmer::Kmer()
{
  static_assert(kmr_sKmerSize <= 32, "kmer size is max 32 base len");
  kmr_isInternalState = IS_NORMAL_NORMAL;
}

Kmer::Kmer(const std::string &strData)
{
  kmr_bkKmerBinaryRepresentation = 0;
  for (const char cElem : strData) {
    kmr_bkKmerBinaryRepresentation <<= 2;
    kmr_bkKmerBinaryRepresentation |= CharToBin(cElem);
  }
  kmr_isInternalState = IS_NORMAL_NORMAL;
  CacheTrueKmerValue();
}

Kmer::Kmer(const twobit cPrefix, const Kmer& kmrOther)
{
  kmr_isInternalState = kmrOther.kmr_isInternalState;
  switch (kmrOther.kmr_isInternalState)
  {
  case IS_NORMAL_NORMAL:
    AddPrefix(cPrefix, kmrOther);
    break;
  case IS_NORMAL_REVERSED:
    AddSuffix(kmrOther, cPrefix);
    break;
  case IS_INVERTED_NORMAL:
    AddPrefix(InvertSequenceChar(cPrefix), kmrOther);
    break;
  default:
    AddSuffix(kmrOther, InvertSequenceChar(cPrefix));
    break;
  }
}

Kmer::Kmer(const Kmer& kmrOther, const twobit cSuffix)
{
  kmr_isInternalState = kmrOther.kmr_isInternalState;
  switch (kmrOther.kmr_isInternalState)
  {
  case IS_NORMAL_NORMAL:
    AddSuffix(kmrOther, cSuffix);
    break;
  case IS_NORMAL_REVERSED:
    AddPrefix(cSuffix, kmrOther);
    break;
  case IS_INVERTED_NORMAL:
    AddSuffix(kmrOther, InvertSequenceChar(cSuffix));
    break;
  default:
    AddPrefix(InvertSequenceChar(cSuffix), kmrOther);
    break;
  }
}

Kmer::Kmer(const Kmer & kmrOther)
{
  kmr_isInternalState = kmrOther.kmr_isInternalState;
  kmr_bkKmerBinaryRepresentation = kmrOther.kmr_bkKmerBinaryRepresentation;
}

Kmer::Kmer(const ullint ullKmerBits)
{
  kmr_isInternalState = IS_NORMAL_NORMAL;
  kmr_bkKmerBinaryRepresentation = ullKmerBits;
  CacheTrueKmerValue();
}

Kmer::~Kmer()
{
}

int Kmer::Size() const
{
  return kmr_sKmerSize;
}

std::string Kmer::ToString() const
{
  std::string strKmer;
  const Kmer &kmrKmer = *this;
  for (int i = 0; i < Size(); i++) {
    strKmer.push_back(BinToChar(kmrKmer[i]));
  }
  return strKmer;
}

std::string Kmer::ToStringNormalized() const
{
  std::string strRet = ToString();
  InternalState isPrintingState = kmr_isInternalState;

  if (isPrintingState == IS_NORMAL_REVERSED || isPrintingState == IS_INVERTED_REVERSED) {
    isPrintingState = isPrintingState == IS_NORMAL_REVERSED ? IS_NORMAL_NORMAL : isPrintingState;
    isPrintingState = isPrintingState == IS_INVERTED_REVERSED ? IS_INVERTED_NORMAL : isPrintingState;
    std::reverse(strRet.begin(), strRet.end());
  }

  if (isPrintingState == IS_INVERTED_NORMAL) {
    strRet = InvertSequence(strRet);
  }
  return strRet;
}

ullint Kmer::ToUllint() const
{
  return kmr_bkKmerBinaryRepresentation;
}

Kmer& Kmer::operator=(const Kmer & kmrOther)
{
  kmr_bkKmerBinaryRepresentation = kmrOther.kmr_bkKmerBinaryRepresentation;
  kmr_isInternalState = kmrOther.kmr_isInternalState;
  return *this;
}

bool Kmer::operator<(const Kmer & kmr_Other) const
{
  return kmr_bkKmerBinaryRepresentation < kmr_Other.kmr_bkKmerBinaryRepresentation;
}

twobit Kmer::operator[](const int iIndex) const
{
  int iShift = 31 * 2 - 2 * iIndex - 2;
  ullint ullRetVal = kmr_bkKmerBinaryRepresentation & (0x3ull << iShift);
  ullRetVal >>= iShift;
  return (twobit)ullRetVal;
}

bool Kmer::operator==(const Kmer & kmrOther) const
{
  return !(*this < kmrOther) && !(kmrOther < *this);
}

bool Kmer::operator!=(const Kmer & kmrOther) const
{
  return !(*this == kmrOther);
}

static ullint ReverseSequence(const ullint ullIn, const short sKmerSize) {
  ullint ullRet = 0;
  for (int i = 0; i < sKmerSize; i++) {
    ullRet <<= 2;
    ullRet |= (ullIn & (0x3ll << (2 * i))) >> (2 * i);
  }

  return ullRet;
}

static ullint InvertSequence(const ullint ullIn, const short sKmerSize) {
  ullint ullRet = 0;
  for (int i = 0; i < sKmerSize; i++) {
    ullRet |= ~(ullIn & (0x3ll << (2 * i))) & (0x3ll << (2 * i));
  }

  return ullRet;
}

binKmer Kmer::CacheTrueKmerValue()
{
  ullint ullRet = kmr_bkKmerBinaryRepresentation;

  InternalState isState = IS_NORMAL_NORMAL;

  ullint ullSmallestRet = ullRet;
  
  ullint ullTemp = InvertSequence(ullSmallestRet, kmr_sKmerSize);
  isState = ullSmallestRet < ullTemp ? isState : IS_INVERTED_NORMAL;
  ullSmallestRet = ullSmallestRet < ullTemp ? ullSmallestRet : ullTemp;
  
  ullTemp = ReverseSequence(ullTemp, kmr_sKmerSize);
  isState = ullSmallestRet < ullTemp ? isState : IS_INVERTED_REVERSED;
  ullSmallestRet = ullSmallestRet < ullTemp ? ullSmallestRet : ullTemp;

  ullTemp = InvertSequence(ullTemp, kmr_sKmerSize);
  isState = ullSmallestRet < ullTemp ? isState : IS_NORMAL_REVERSED;
  ullSmallestRet = ullSmallestRet < ullTemp ? ullSmallestRet : ullTemp;

  kmr_bkKmerBinaryRepresentation = ullSmallestRet;

  static const int aaiMapping[4][4] = { { IS_NORMAL_NORMAL, IS_NORMAL_REVERSED, IS_INVERTED_NORMAL, IS_INVERTED_REVERSED },
                                        { IS_NORMAL_REVERSED, IS_NORMAL_NORMAL, IS_INVERTED_REVERSED, IS_INVERTED_NORMAL },
                                        { IS_INVERTED_NORMAL, IS_INVERTED_REVERSED, IS_NORMAL_NORMAL, IS_NORMAL_REVERSED },
                                        { IS_INVERTED_REVERSED, IS_INVERTED_NORMAL, IS_NORMAL_REVERSED, IS_NORMAL_NORMAL} };

  kmr_isInternalState = (InternalState)aaiMapping[isState][kmr_isInternalState];
  kmr_bkKmerBinaryRepresentation = ullSmallestRet;
  return ullSmallestRet;
}

void Kmer::AddPrefix(const twobit cPrefix, const Kmer & kmrOther)
{
  kmr_bkKmerBinaryRepresentation = kmrOther.kmr_bkKmerBinaryRepresentation >> 2;
  kmr_bkKmerBinaryRepresentation |= ((ullint)cPrefix) << (2 * (Kmer::kmr_sKmerSize - 1));

  CacheTrueKmerValue();
}

void Kmer::AddSuffix(const Kmer & kmrOther, const twobit cSuffix)
{
  kmr_bkKmerBinaryRepresentation = (kmrOther.kmr_bkKmerBinaryRepresentation << 2) & ~(0x3ull << (2 * (Kmer::kmr_sKmerSize)));
  kmr_bkKmerBinaryRepresentation |= cSuffix;

  CacheTrueKmerValue();
}

std::ostream& operator<<(std::ostream& ostStream, const Kmer& kmrKmer)
{
  return ostStream << kmrKmer.ToString() << "\n";
}

#include "SpaceEfficientMultiGraphBasedOnBloomFilter.h"
#include "DeBrujinGraphComplexKmerNode.h"


SpaceEfficientMultiGraphBasedOnBloomFilter::SpaceEfficientMultiGraphBasedOnBloomFilter(const Kmer& kmrRandomStartingKmer, const KmerTraverser& ktTraverser) :
  mgr_kmrStartingKmer(kmrRandomStartingKmer), mgr_ktTreverser(ktTraverser)
{
}

SpaceEfficientMultiGraphBasedOnBloomFilter::~SpaceEfficientMultiGraphBasedOnBloomFilter()
{
  for (uint i = 0; i < mgr_vCreatedNodes.size(); i++) {
    delete mgr_vCreatedNodes[i];
    mgr_vCreatedNodes[i] = nullptr;
  }
}

Node* SpaceEfficientMultiGraphBasedOnBloomFilter::CreateNewNode(const Kmer kmrKmer1, const Kmer kmrKmer2, const Direction dirDir1, const Direction dirDir2, const uint uiLength)
{
  DeBrujinGraphComplexKmerNode* cnoNewNode = nullptr;

  if (kmrKmer2 < kmrKmer1) {
    cnoNewNode = new DeBrujinGraphComplexKmerNode(kmrKmer2, kmrKmer1, dirDir2, dirDir1, uiLength);
  }
  else {
    cnoNewNode = new DeBrujinGraphComplexKmerNode(kmrKmer1, kmrKmer2, dirDir1, dirDir2, uiLength);
  }

  /*if (kmrKmer1 == Kmer("CATGTTACAACTTATTAATAAGTTGTAACAT") && kmrKmer2 == Kmer("CGCACCAAGATAACGTACAATGTTGAATAAT") && uiLength == 14) {
    printf("%s %s\n", kmrKmer1.ToString(), kmrKmer2.ToString());
  }*/

  mgr_vCreatedNodes.push_back(cnoNewNode);
  return cnoNewNode;
}

Node * SpaceEfficientMultiGraphBasedOnBloomFilter::CreateNewNode(const Kmer & kmrCurrentKmer)
{
  // simple node case
  if (mgr_ktTreverser.IsSimpleKmer(kmrCurrentKmer)) {
    std::vector<Direction> vDirestions = mgr_ktTreverser.GetNeighbours(kmrCurrentKmer);
    assert(vDirestions.size() == 2);

    uint uiLength = 0;

    const Direction dirDir1 = vDirestions[0];
    const Direction dirDir2 = vDirestions[1];

    Kmer kmrKmer1 = KmerTraverser::NextKmer(kmrCurrentKmer, dirDir1);
    Kmer kmrKmer2 = KmerTraverser::NextKmer(kmrCurrentKmer, dirDir2);

    const bool bSuccessedInExpansionOfKmer1 = kmrCurrentKmer != kmrKmer1;
    const bool bSuccessedInExpansionOfKmer2 = kmrCurrentKmer != kmrKmer2;

    kmrKmer1 = bSuccessedInExpansionOfKmer1 ? kmrKmer1 : kmrCurrentKmer;
    kmrKmer2 = bSuccessedInExpansionOfKmer2 ? kmrKmer2 : kmrCurrentKmer;

    Direction dirDirPrev1 = DIR_NONE;
    Direction dirDirPrev2 = DIR_NONE;

    if (bSuccessedInExpansionOfKmer1) {
      uiLength++;
      dirDirPrev1 = mgr_ktTreverser.GetDirection(kmrKmer1, kmrCurrentKmer);
      mgr_ktTreverser.DirectTraverse(kmrKmer1, dirDirPrev1, uiLength);
    }

    if (bSuccessedInExpansionOfKmer2) {
      uiLength++;
      dirDirPrev2 = mgr_ktTreverser.GetDirection(kmrKmer2, kmrCurrentKmer);
      mgr_ktTreverser.DirectTraverse(kmrKmer2, dirDirPrev2, uiLength);
    }

    const Direction dirNodeDir1 = bSuccessedInExpansionOfKmer1 ? dirDirPrev1 : dirDir2;
    const Direction dirNodeDir2 = bSuccessedInExpansionOfKmer2 ? dirDirPrev2 : dirDir1;

    Direction dirNode1TrueDir = dirNodeDir1;
    if (kmrKmer1 == kmrKmer2 && (dirDirPrev1 != DIR_NONE)) {
      const Kmer kmrTmp = kmrKmer1;
      kmrKmer1 = KmerTraverser::NextKmer(kmrKmer1, dirDirPrev1);
      dirNode1TrueDir = mgr_ktTreverser.GetDirection(kmrKmer1, kmrTmp);
    }

    return CreateNewNode(kmrKmer1, kmrKmer2, dirNode1TrueDir, dirNodeDir2, uiLength);
  }
  // complex node
  else {
    std::vector<Direction> vDirestions = mgr_ktTreverser.GetNeighbours(kmrCurrentKmer);

    uint uiLength = 1;
    Direction dirDir1 = vDirestions[0];
    Kmer kmrNeighbour = KmerTraverser::NextKmer(kmrCurrentKmer, dirDir1);
    Direction dirDir2 = mgr_ktTreverser.GetDirection(kmrNeighbour, kmrCurrentKmer);

    mgr_ktTreverser.DirectTraverse(kmrNeighbour, dirDir2, uiLength);

    return CreateNewNode(kmrCurrentKmer, kmrNeighbour, dirDir1, dirDir2, uiLength);
  }
}

static Direction AreKmersNeighbours(const Kmer& kmrKmerA, const Kmer& kmrKmerB) {
  // for every direction 
  for (uint i = 0; i < 8; i++) {
    Direction dirDir = (Direction)i;
    if (kmrKmerA == KmerTraverser::NextKmer(kmrKmerB, dirDir)) {
      return dirDir;
    }
  }

  return DIR_NONE;
}

void SpaceEfficientMultiGraphBasedOnBloomFilter::GetNeighboursForKmer(const Kmer& kmrBaseKmer, const Direction& dirDontExpand, std::vector<Node*>& vOtherKmerNeighbours)
{
  std::vector<Direction> vBaseKmerNeighbours = mgr_ktTreverser.GetNeighbours(kmrBaseKmer);
  std::vector<Direction> vDontExpand;

  for (Direction dirDir : vBaseKmerNeighbours) {

    bool bCycleSkip = false;
    for (Direction dirDontExpand : vDontExpand) {
      if (dirDir == dirDontExpand) {
        bCycleSkip = true;
        break;
      }
    }

    if (bCycleSkip) continue;
    if (dirDir == dirDontExpand) continue;
    if (IsVisited(kmrBaseKmer, dirDir)) continue;

    Kmer kmrNeighbour = KmerTraverser::NextKmer(kmrBaseKmer, dirDir);
    if (kmrNeighbour == kmrBaseKmer) continue;
    Direction dirBackFromNeighbour = mgr_ktTreverser.GetDirection(kmrNeighbour, kmrBaseKmer);
    uint uiLength = 1;

    mgr_ktTreverser.DirectTraverse(kmrNeighbour, dirBackFromNeighbour, uiLength);

    // cycle case: cycle neighbour node should not be traversed two time 
    Direction dirCycleSecondDir = AreKmersNeighbours(kmrBaseKmer, kmrNeighbour);
    if (dirCycleSecondDir != DIR_NONE && uiLength > 1) {
      vDontExpand.push_back(dirCycleSecondDir);
    }

    vOtherKmerNeighbours.push_back(CreateNewNode(kmrBaseKmer, kmrNeighbour, dirDir, dirBackFromNeighbour, uiLength));
  }
}

std::vector<Node*> SpaceEfficientMultiGraphBasedOnBloomFilter::GetNeighbors(const Node& noNodeNotCasted)
{
  DEBRUJIN_NODE noNode = (DEBRUJIN_NODE)noNodeNotCasted;
  std::vector<Node*> vAllNotVisitedNodes;

  GetNeighboursForKmer(noNode.cno_kmrKmer1, noNode.cno_dirDir1, vAllNotVisitedNodes);
  GetNeighboursForKmer(noNode.cno_kmrKmer2, noNode.cno_dirDir2, vAllNotVisitedNodes);

  return vAllNotVisitedNodes;
}

bool SpaceEfficientMultiGraphBasedOnBloomFilter::IsVisited(const Kmer & kmrKmer, const Direction dirDirection) const
{
  return mgr_sVisited.find(std::make_pair(kmrKmer, dirDirection)) != mgr_sVisited.end();
}

bool SpaceEfficientMultiGraphBasedOnBloomFilter::IsVisited(const Node & noNode) const
{
  return IsVisited(((DEBRUJIN_NODE)noNode).cno_kmrKmer1, ((DEBRUJIN_NODE)noNode).cno_dirDir1);
}

void SpaceEfficientMultiGraphBasedOnBloomFilter::MarkVisited(const Node & noNode, bool bVisited)
{
  std::pair<Kmer, Direction> pNodeDefinition1 = std::make_pair(((DEBRUJIN_NODE)noNode).cno_kmrKmer1, ((DEBRUJIN_NODE)noNode).cno_dirDir1);
  std::pair<Kmer, Direction> pNodeDefinition2 = std::make_pair(((DEBRUJIN_NODE)noNode).cno_kmrKmer2, ((DEBRUJIN_NODE)noNode).cno_dirDir2);

  if (bVisited) {
    mgr_sVisited.insert(pNodeDefinition1);
    mgr_sVisited.insert(pNodeDefinition2);
  }
  else {
    mgr_sVisited.erase(pNodeDefinition1);
    mgr_sVisited.erase(pNodeDefinition2);
  }
}

Node * SpaceEfficientMultiGraphBasedOnBloomFilter::GetStartingNotVisitedNode() 
{
  Node* nSomeNode = CreateNewNode(mgr_kmrStartingKmer);
  return nSomeNode;
}



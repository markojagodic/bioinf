#include "BFSGraphTraversal.h"

#include <queue>

BFSGraphTraversal::BFSGraphTraversal()
{
}


BFSGraphTraversal::~BFSGraphTraversal()
{
}


struct MyComparator
{
  bool operator()(const std::pair<ullint, Node*>& _Left, const std::pair<ullint, Node*>& _Right) const
  {
    if (_Left.first == _Right.first) {
      return *_Left.second < *_Right.second;
    }

    return _Left.first > _Right.first;
  }
};

std::vector<Node*> BFSGraphTraversal::TraverseGraph(MultiGraph & gGraph)
{
  Node *noStartNode = gGraph.GetStartingNotVisitedNode();

  std::priority_queue<std::pair<ullint, Node*>, std::vector<std::pair<ullint, Node*>>, MyComparator> qOpenNodes;
  qOpenNodes.push(std::make_pair(0, noStartNode));
  gGraph.MarkVisited(*noStartNode, true);
  
  std::unordered_map<Node*, ullint> umnsDistace;
  umnsDistace[noStartNode] = 0;
  std::pair<Node*, ullint> pnulMaxDistanceNode = std::make_pair(noStartNode, 0);
  
  while (!qOpenNodes.empty()) {
    std::pair<ullint, Node*> pnoCurrentNode = qOpenNodes.top();
    qOpenNodes.pop();
    ullint ullCurrentDistance = umnsDistace[pnoCurrentNode.second];

    for (Node * pnoNode : gGraph.GetNeighbors(*pnoCurrentNode.second)) {
      Node& noNode = *pnoNode;
      if (gGraph.IsVisited(noNode)) continue;
      gGraph.MarkVisited(noNode, true);

      if (umnsDistace.find(pnoNode) == umnsDistace.end()) {
        umnsDistace[pnoNode] = ullCurrentDistance + noNode.Length();
      }
      else if (umnsDistace[pnoNode] > ullCurrentDistance + noNode.Length()) {
        umnsDistace[pnoNode] = ullCurrentDistance + noNode.Length();
      }

      if (umnsDistace[pnoNode] > pnulMaxDistanceNode.second) {
        pnulMaxDistanceNode.second = umnsDistace[pnoNode];
        pnulMaxDistanceNode.first = pnoNode;
      }

      qOpenNodes.push(std::make_pair(umnsDistace[pnoNode], pnoNode));
    }
  }

  //for (auto elem : umnsDistace) {
  //  elem.first->PrintNode();
  //}

  for (auto elem : umnsDistace) {
    gGraph.MarkVisited(*elem.first, false);
  }

  umnsDistace.clear();

  qOpenNodes.push(std::make_pair(0, pnulMaxDistanceNode.first));
  gGraph.MarkVisited(*pnulMaxDistanceNode.first, true);
  umnsDistace[pnulMaxDistanceNode.first] = 0;

  pnulMaxDistanceNode = std::make_pair(pnulMaxDistanceNode.first, 0);

  std::unordered_map<Node*, Node*> mParenthood;

  while (!qOpenNodes.empty()) {
    std::pair<ullint, Node*> pnoCurrentNode = qOpenNodes.top();
    qOpenNodes.pop();
    ullint ullCurrentDistance = umnsDistace[pnoCurrentNode.second];

    for (Node * pnoNode : gGraph.GetNeighbors(*pnoCurrentNode.second)) {
      Node& noNode = *pnoNode;
      if (gGraph.IsVisited(noNode)) continue;
      gGraph.MarkVisited(noNode, true);

      if (umnsDistace.find(pnoNode) == umnsDistace.end()) {
        umnsDistace[pnoNode] = ullCurrentDistance + noNode.Length();
      }
      else if (umnsDistace[pnoNode] > ullCurrentDistance + noNode.Length()) {
        umnsDistace[pnoNode] = ullCurrentDistance + noNode.Length();
      }

      if (umnsDistace[pnoNode] > pnulMaxDistanceNode.second) {
        pnulMaxDistanceNode.second = umnsDistace[pnoNode];
        pnulMaxDistanceNode.first = pnoNode;
      }

      qOpenNodes.push(std::make_pair(umnsDistace[pnoNode], pnoNode));

      mParenthood[pnoNode] = pnoCurrentNode.second;
    }
  }
  
  //for (auto elem : umnsDistace) {
  //  elem.first->PrintNode();
  //}

  return BFSPathReconstructor::Reconstruct(mParenthood, pnulMaxDistanceNode.first);
}

std::vector<Node*> BFSPathReconstructor::Reconstruct(std::unordered_map<Node*, Node*> &mParenthood, Node * pnoStart)
{
  std::vector<Node*> aNodesRet;
  Node* pnoCurrent = pnoStart;
  while (pnoCurrent != nullptr) {
    aNodesRet.push_back(pnoCurrent);
    pnoCurrent = mParenthood[pnoCurrent];
  }
  return aNodesRet;
}

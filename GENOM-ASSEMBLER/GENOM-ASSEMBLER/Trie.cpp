#include "Trie.h"

#include <cstring>



Trie::Trie()
{
  t_teElement = nullptr;
}


Trie::~Trie()
{
}

TrieElem* Trie::GetRoot()
{
  if (t_teElement == nullptr) {
    t_teElement = new TrieElem();
  }

  return t_teElement;
}

TrieElem::TrieElem()
{
  memset(te_pteElems, 0, sizeof te_pteElems);
  te_bEnding = 0;
}

TrieElem::~TrieElem()
{
}

TrieElem * TrieElem::InsertNext(int iNext)
{
  if (te_pteElems[iNext] == nullptr) {
    te_pteElems[iNext] = new TrieElem();
  }

  return te_pteElems[iNext];
}

void TrieElem::EndInsertion()
{
  te_bEnding++;
}

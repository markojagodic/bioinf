#pragma once

class TrieElem {
  int te_bEnding;
  TrieElem *te_pteElems[4];

public:
  TrieElem();
  ~TrieElem();

  TrieElem* InsertNext(int iNext);
  void EndInsertion();
};

class Trie
{
  TrieElem *t_teElement;
public:
  Trie();
  ~Trie();

  TrieElem* GetRoot();
};


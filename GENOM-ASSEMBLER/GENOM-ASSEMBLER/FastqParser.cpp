#include "FastqParser.h"
#include "SequencingReads.h"
#include <string>
#include <fstream>
#include <iostream>

/* 
Takes file in fastq format and extracts informations and reads from it. 
All extracted informations are stored in SequencingReads. 

Fastq file is expected to have number of lines divisible by 4 
For more details about fastq format see https://en.wikipedia.org/wiki/FASTQ_format
*/
SequencingReads FastqParser::ParseFile(const std::string strFileName)
{
  // returning reads
  SequencingReads fpr_srReads;
 
  // informations extracted from 4 lines of fastq format
  std::string strDescription1;
  std::string strSequence;
  std::string strDescription2;
  std::string strQuality;

  std::ifstream ifsFileStreamTmp;
  if (strFileName != "") {
    ifsFileStreamTmp = std::ifstream(fqp_strDatasetPath + strFileName);
    if(!ifsFileStreamTmp.is_open()){ 
      printf("Can't open desired file!");
    }
  }

  std::istream& ifsFileStream = ifsFileStreamTmp.is_open() ? ifsFileStreamTmp : std::cin;
  
  std::string line;
  {
    char lineNumber = 0;
    bool error = false;

    while (std::getline(ifsFileStream, line)) 
    {
      switch (lineNumber)
      {
      case 0:
        if (line[0] != '@') 
          error = true;
        else 
          strDescription1 = line;
        break;
      case 1:
        strSequence = line;
        break;
      case 2:
        if (line[0] != '+')

          error = true;
        else
          strDescription2 = line;
        break;
      case 3:
        strQuality = line;
        break;
      default:
        error = true;
        break;
      }

      if (lineNumber == 3) 
      {
        if (strSequence.length() != strQuality.length()) 
        {
          //error = true;
        }

        if (error)
        {
          printf("Error occur while parsing");
          //exit(EXIT_FAILURE);
        }
        fpr_srReads.addRead(strDescription1, strSequence, strDescription2, strQuality);
      }

      lineNumber++;
      lineNumber %= 4;
    }
  }

  return fpr_srReads;
}


//SequencingReads FastqParser::ParseFile(const char * acFileName)
//{
//  const int BUFFER_SIZE = 1024 * 1024;
//
//  FILE *fp; // = fopen(fqp_acDatasetPath + *acFileName, "rb");
//  char buffer[BUFFER_SIZE];
//  ullint ullBufferIndex = 0;
//
//
//
//
//  return SequencingReads();
//}


#pragma once

#include <string>

#include "HelperFunctions.h"
#include "Kmer.h"

class Kmer;

class SequenceData
{
  ullint sd_ullNumElements;
  ullint sd_ullByteSize;
  uchar *sd_pacGenomData;
public:
  SequenceData(std::string strData);
  SequenceData(ullint ullData);
  SequenceData(const SequenceData &sdOriginal);
  SequenceData(const twobit cPrefix, const Kmer& kmrOther);
  SequenceData(const Kmer& kmrOther, const twobit cSuffix);
  ~SequenceData();

  SequenceData& operator = (const SequenceData &sdOther);

  uchar operator [] (ullint ullIndex) const;

  ullint Size() const;
  std::string ToString();
};


#pragma once
#include "GraphTraversalAlgorithm.h"
#include <unordered_set>
#include <unordered_map>

#include "DeBrujinGraphComplexKmerNode.h"

class BFSGraphTraversal :
  public GraphTraversalAlgorithm
{
public:
  BFSGraphTraversal();
  virtual ~BFSGraphTraversal();
  
  virtual std::vector<Node*> TraverseGraph(MultiGraph & gGraph) override;
};


class BFSPathReconstructor {
public:
  static std::vector<Node*> Reconstruct(std::unordered_map<Node*, Node*> &mParenthood, Node * pnoStart);
};
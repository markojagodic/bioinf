#pragma once

#ifndef HASH_H
#define HASH_H


#include "Kmer.h"
#include <array>
#include <utility>
#include "HelperFunctions.h"

class Hash
{
  const static std::array<std::pair<ullint, ullint>, 9> h_apCoefficients;

public:
  static ullint GetHash(const Kmer& kmrKmer, int iVersion = 0);
};

#endif
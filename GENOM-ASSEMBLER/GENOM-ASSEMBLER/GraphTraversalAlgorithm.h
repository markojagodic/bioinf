#pragma once

#include "SequenceData.h"
#include "Graph.h"

class GraphTraversalAlgorithm
{
public:
  GraphTraversalAlgorithm();
  virtual ~GraphTraversalAlgorithm();

  virtual std::vector<Node*> TraverseGraph(MultiGraph &gGraph) = 0;
};


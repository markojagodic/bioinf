#include "Test.h"

#include "Kmer.h"
#include "BloomFilter.h"
#include "BFSGraphTraversal.h"
#include "CriticalFalsePositiveSet.h"
#include "KmerTraverser.h"
#include "SpaceEfficientMultiGraphBasedOnBloomFilter.h"
#include "ReadData.h"

#include <vector>
#include <assert.h>

// ~G = C
// ~T = A

static std::vector<Kmer> Factory(std::vector<std::string> vSequences)
{
  std::vector<Kmer> vRet;

  for (std::string& strSeq : vSequences) {
    ReadData rdNewRead(strSeq);

    for (Kmer kmrKmer : rdNewRead) {
      vRet.push_back(kmrKmer);
    }
  }

  return vRet;
}


void TestKmer() {
  Kmer a("ACTGAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  Kmer b("AAAAAAAAAAAAAAAAAAAAAAAAAAAGTCA");
  Kmer c("TGACTTTTTTTTTTTTTTTTTTTTTTTTTTT");
  Kmer d("TTTTTTTTTTTTTTTTTTTTTTTTTTTCAGT");
  Kmer e("ACTGAAAAAAAAAAAAAAAAAAAAAAAAAAA");

  assert(a.ToString() == "ACTGAAAAAAAAAAAAAAAAAAAAAAAAAAA");

  assert(a == e);

  assert(a == b);
  assert(a == c);
  assert(a == d);
  assert(b == c);
  assert(b == d);
  assert(c == d);

  assert(a.ToUllint() == e.ToUllint());

  assert(a.ToUllint() == b.ToUllint());
  assert(a.ToUllint() == c.ToUllint());
  assert(a.ToUllint() == d.ToUllint());
  assert(b.ToUllint() == c.ToUllint());
  assert(b.ToUllint() == d.ToUllint());
  assert(c.ToUllint() == d.ToUllint());


  a = Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  b = Kmer(a, CharToBin('T'));
  c = Kmer(CharToBin('T'), a);

  assert(b == Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAT"));
  assert(c == Kmer("TAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
}

void TheoryTest() {
  Kmer kmrStar("TGAAAAAAAAAAAAAAAAAAAAAAAAAAACC");
                
  Kmer d1, d2, d3, d4, d5, d6, d7, d8;
  d1 = Kmer(kmrStar, CharToBin('A'));
  d2 = Kmer(CharToBin('A'), kmrStar);
  d3 = Kmer(kmrStar, CharToBin('C'));
  d4 = Kmer(CharToBin('C'), kmrStar);
  d5 = Kmer(kmrStar, CharToBin('T'));
  d6 = Kmer(CharToBin('T'), kmrStar);
  d7 = Kmer(kmrStar, CharToBin('G'));
  d8 = Kmer(CharToBin('G'), kmrStar);


  /*printf("0 %s\n", kmrStar.ToString().c_str());
  printf("1 %s\n", d1.ToString().c_str());
  printf("2 %s\n", d2.ToString().c_str());
  printf("3 %s\n", d3.ToString().c_str());
  printf("4 %s\n", d4.ToString().c_str());
  printf("5 %s\n", d5.ToString().c_str());
  printf("6 %s\n", d6.ToString().c_str());
  printf("7 %s\n", d7.ToString().c_str());
  printf("8 %s\n", d8.ToString().c_str());*/
}


void TestGraphWithoutCycleTraverseWithSimpleNodes() {
  std::vector<std::string> vSequences = {
    "AAAAAAAAAAAAAAGAAAAAAAAAAAAAAATGACG"
  };

  std::vector<Kmer> vKmers = Factory(vSequences);

  for (int i = 0; i < vKmers.size(); i++) {
    for (int j = i + 1; j < vKmers.size(); j++) {
      Kmer& kmrKmer1 = vKmers[i];
      Kmer& kmrKmer2 = vKmers[j];

      assert(!(kmrKmer1 == kmrKmer2));
    }
  }

  //{
  //  for (const Kmer k : vKmers) {
  //    PrintAllFourVersions(k.ToString());
  //  }
  //}

  BloomFilter bfFilter = BloomFilter(3);
  bfFilter.AddVector(vKmers);
  CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
  KmerTraverser ktTraverser(bfFilter, cfpSet);
  SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
  BFSGraphTraversal bfsTraversal;
  std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);
  assert(CompareAllCombosSequences(ktTraverser.PathToString(aNodesPath), "AAAAAAAAAAAAAAGAAAAAAAAAAAAAAATGACG"));
  //ktTraverser.PathToString(aNodesPath);
}


/*
  inverted normal overlap

  -------------
      $$$$$$$$$
      ----------------

*/
void TestGraphWithoutCycleSimpleInvertedOverlap() {
  std::vector<Kmer> vKmers;
  vKmers.push_back(Kmer("AAAAAAAAAAAAAAGAAAAAAAAAAAAAAAT"));
  vKmers.push_back(Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATG"));
  vKmers.push_back(Kmer("TTTTTTTTTTTTTCTTTTTTTTTTTTTTTAC"));
  vKmers.push_back(Kmer("TTTTTTTTTTTTCTTTTTTTTTTTTTTTACG"));

  //for (int i = 0; i < vKmers.size(); i++) {
  //  for (int j = i + 1; j < vKmers.size(); j++) {
  //    Kmer& kmrKmer1 = vKmers[i];
  //    Kmer& kmrKmer2 = vKmers[j];

  //    assert(!(kmrKmer1 == kmrKmer2));
  //  }
  //}

  BloomFilter bfFilter = BloomFilter(3);
  bfFilter.AddVector(vKmers);
  CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
  KmerTraverser ktTraverser(bfFilter, cfpSet);
  SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
  BFSGraphTraversal bfsTraversal;
  std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);
  ktTraverser.PathToString(aNodesPath);
  //assert(CompareAllCombosSequences(ktTraverser.PathToString(aNodesPath), "AAAAAAAAAAAAAAGAAAAAAAAAAAAAAATGC"));
}


/*
  inverted reversed overlap

   ---------
       $$$$$
       $$$$$
   ---------
*/
void TestGraphWithoutCycleSimpleInvertedReversedOverlap() {
  std::vector<Kmer> vKmers;
  vKmers.push_back(Kmer("AAAAAAAAAAAAAAGAAAAAAAAAAAAAAAT"));
  vKmers.push_back(Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATG"));
  vKmers.push_back(Kmer("TTTTTTTTTTTTTCTTTTTTTTTTTTTTTAC"));
  vKmers.push_back(Kmer("GGTTTTTTTTTTTTCTTTTTTTTTTTTTTTA"));

  //for (int i = 0; i < vKmers.size(); i++) {
  //  for (int j = i + 1; j < vKmers.size(); j++) {
  //    Kmer& kmrKmer1 = vKmers[i];
  //    Kmer& kmrKmer2 = vKmers[j];

  //    assert(!(kmrKmer1 == kmrKmer2));
  //  }
  //}

  BloomFilter bfFilter = BloomFilter(3);
  bfFilter.AddVector(vKmers);
  CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
  KmerTraverser ktTraverser(bfFilter, cfpSet);
  SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
  BFSGraphTraversal bfsTraversal;
  std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);
  ktTraverser.PathToString(aNodesPath);
  //assert(CompareAllCombosSequences(ktTraverser.PathToString(aNodesPath), "AAAAAAAAAAAAAAGAAAAAAAAAAAAAAATGC"));
}

void TestNextKmer() {
  Kmer kmrBase("AAAAAAAAAAAAAAGAAAAAAAAAAAAAAAT");
  assert(KmerTraverser::NextKmer(kmrBase, DIR__C) == Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATC"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_C_) == Kmer("CAAAAAAAAAAAAAAGAAAAAAAAAAAAAAA"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__A) == Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATA"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_A_) == Kmer("AAAAAAAAAAAAAAAGAAAAAAAAAAAAAAA"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__T) == Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATT"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_T_) == Kmer("TAAAAAAAAAAAAAAGAAAAAAAAAAAAAAA"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__G) == Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_G_) == Kmer("GAAAAAAAAAAAAAAGAAAAAAAAAAAAAAA"));

  kmrBase = Kmer("CAGTGTATTCGCCGATATCGTACGATCGATG");
  assert(KmerTraverser::NextKmer(kmrBase, DIR__C) == Kmer("AGTGTATTCGCCGATATCGTACGATCGATGC"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_C_) == Kmer("CCAGTGTATTCGCCGATATCGTACGATCGAT"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__A) == Kmer("AGTGTATTCGCCGATATCGTACGATCGATGA"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_A_) == Kmer("ACAGTGTATTCGCCGATATCGTACGATCGAT"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__T) == Kmer("AGTGTATTCGCCGATATCGTACGATCGATGT"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_T_) == Kmer("TCAGTGTATTCGCCGATATCGTACGATCGAT"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__G) == Kmer("AGTGTATTCGCCGATATCGTACGATCGATGG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_G_) == Kmer("GCAGTGTATTCGCCGATATCGTACGATCGAT"));

  kmrBase = Kmer("GCCCCCCCCCCCCCCCCCCCCCCCCCCTCCG"); 
  assert(kmrBase == Kmer("CGGAGGGGGGGGGGGGGGGGGGGGGGGGGGC")); // reversed and inversed
  assert(KmerTraverser::NextKmer(kmrBase, DIR__C) == Kmer("GCGGAGGGGGGGGGGGGGGGGGGGGGGGGGG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_C_) == Kmer("GGAGGGGGGGGGGGGGGGGGGGGGGGGGGCG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__A) == Kmer("TCGGAGGGGGGGGGGGGGGGGGGGGGGGGGG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_A_) == Kmer("GGAGGGGGGGGGGGGGGGGGGGGGGGGGGCT"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__T) == Kmer("ACGGAGGGGGGGGGGGGGGGGGGGGGGGGGG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_T_) == Kmer("GGAGGGGGGGGGGGGGGGGGGGGGGGGGGCA"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR__G) == Kmer("CCGGAGGGGGGGGGGGGGGGGGGGGGGGGGG"));
  assert(KmerTraverser::NextKmer(kmrBase, DIR_G_) == Kmer("GGAGGGGGGGGGGGGGGGGGGGGGGGGGGCC"));

  kmrBase = Kmer("AGTTTTTGTTTTTTCTTTTTTTTTTTTTTTT");
  kmrBase = KmerTraverser::NextKmer(kmrBase, DIR__T);
  assert(kmrBase == Kmer("GTTTTTGTTTTTTCTTTTTTTTTTTTTTTTT"));
  kmrBase = KmerTraverser::NextKmer(kmrBase, DIR_A_);
  assert(kmrBase == Kmer("AGTTTTTGTTTTTTCTTTTTTTTTTTTTTTT"));

  {
    Kmer kmrWalker("AGTTTTTGTTTTTTCTTTTTTTTTTTTTTTT");
    Kmer kmrKmer0 = kmrWalker;
    Kmer kmrKmer1("GTTTTTGTTTTTTCTTTTTTTTTTTTTTTTA"); // _A ; A_
    Kmer kmrKmer2("TTTTTGTTTTTTCTTTTTTTTTTTTTTTTAA"); // _A ; G_
    Kmer kmrKmer3("TTTTGTTTTTTCTTTTTTTTTTTTTTTTAAC"); // _C ; T_
    Kmer kmrKmer4("TTTGTTTTTTCTTTTTTTTTTTTTTTTAACG"); // _G ; T_

    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__A)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__A)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__C)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__G)) == kmrKmer4);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_G_)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_A_)) == kmrKmer0);
  }

  {
    Kmer kmrWalker("AGTTTTTGTTTTTTCTTTTTTTTTTTTTTCC");
    Kmer kmrKmer0 = kmrWalker;
    Kmer kmrKmer1("GTTTTTGTTTTTTCTTTTTTTTTTTTTTCCA"); // _A ; A_
    Kmer kmrKmer2("TTTTTGTTTTTTCTTTTTTTTTTTTTTCCAA"); // _A ; G_
    Kmer kmrKmer3("TTTTGTTTTTTCTTTTTTTTTTTTTTCCAAC"); // _C ; T_
    Kmer kmrKmer4("TTTGTTTTTTCTTTTTTTTTTTTTTCCAACG"); // _G ; T_

    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__A)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__A)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__C)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__G)) == kmrKmer4);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_G_)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_A_)) == kmrKmer0);
  }

  {
    Kmer kmrWalker("GGTTTTTGTTTTTTCTTTTTTTTTTTTTTGG");
    Kmer kmrKmer0 = kmrWalker;
    Kmer kmrKmer1("GTTTTTGTTTTTTCTTTTTTTTTTTTTTGGA"); // _A ; G_
    Kmer kmrKmer2("TTTTTGTTTTTTCTTTTTTTTTTTTTTGGAT"); // _T ; G_
    Kmer kmrKmer3("TTTTGTTTTTTCTTTTTTTTTTTTTTGGATC"); // _C ; T_
    Kmer kmrKmer4("TTTGTTTTTTCTTTTTTTTTTTTTTGGATCG"); // _G ; T_

    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__A)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__T)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__C)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__G)) == kmrKmer4);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_G_)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_G_)) == kmrKmer0);
  }

  {
    Kmer kmrWalker("CCGTCTTTTGTTTTTTCTTTTTTTTTTTTTT");
    Kmer kmrKmer0 = kmrWalker;
    Kmer kmrKmer1("CGTCTTTTGTTTTTTCTTTTTTTTTTTTTTA"); // _A ; C_
    Kmer kmrKmer2("GTCTTTTGTTTTTTCTTTTTTTTTTTTTTAT"); // _T ; C_
    Kmer kmrKmer3("TCTTTTGTTTTTTCTTTTTTTTTTTTTTATC"); // _C ; G_
    Kmer kmrKmer4("CTTTTGTTTTTTCTTTTTTTTTTTTTTATCG"); // _G ; T_

    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__A)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__T)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__C)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR__G)) == kmrKmer4);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_T_)) == kmrKmer3);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_G_)) == kmrKmer2);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_C_)) == kmrKmer1);
    assert((kmrWalker = KmerTraverser::NextKmer(kmrWalker, DIR_C_)) == kmrKmer0);
  }
}


void TestGraphWithoutCycleTraverseWithComplexNodes() {
  std::vector<Kmer> vKmers;
  vKmers.push_back(Kmer("AAAAAAAAAAAAAAGAAAAAAAAAAAAAAAT"));
  vKmers.push_back(Kmer("AAAAAAAAAAAAAGAAAAAAAAAAAAAAATG"));
  vKmers.push_back(Kmer("AAAAAAAAAAAAGAAAAAAAAAAAAAAATGA"));
  vKmers.push_back(Kmer("AAAAAAAAAAAGAAAAAAAAAAAAAAATGAC"));
  vKmers.push_back(Kmer("AAAAAAAAAAGAAAAAAAAAAAAAAATGACG"));

  //vKmers.push_back(Kmer("AAAAAAAAAAGAAAAAAAAAAAAAAATGACG"));
  vKmers.push_back(Kmer("AAAAAAAAAGAAAAAAAAAAAAAAATGACGA"));
  vKmers.push_back(Kmer("AAAAAAAAGAAAAAAAAAAAAAAATGACGAT"));
  vKmers.push_back(Kmer("AAAAAAAGAAAAAAAAAAAAAAATGACGATT"));
  vKmers.push_back(Kmer("AAAAAAGAAAAAAAAAAAAAAATGACGATTG"));
  vKmers.push_back(Kmer("AAAAAGAAAAAAAAAAAAAAATGACGATTGT"));

  //vKmers.push_back(Kmer("AAAAAAAAAAGAAAAAAAAAAAAAAATGACG"));
  vKmers.push_back(Kmer("AAAAAAAAAGAAAAAAAAAAAAAAATGACGT"));
  vKmers.push_back(Kmer("AAAAAAAAGAAAAAAAAAAAAAAATGACGTG"));
  vKmers.push_back(Kmer("AAAAAAAGAAAAAAAAAAAAAAATGACGTGT"));
  vKmers.push_back(Kmer("AAAAAAGAAAAAAAAAAAAAAATGACGTGTA"));
  vKmers.push_back(Kmer("AAAAAGAAAAAAAAAAAAAAATGACGTGTAG"));
  vKmers.push_back(Kmer("AAAAGAAAAAAAAAAAAAAATGACGTGTAGT"));

  for (int i = 0; i < vKmers.size(); i++) {
    for (int j = i + 1; j < vKmers.size(); j++) {
      Kmer& kmrKmer1 = vKmers[i];
      Kmer& kmrKmer2 = vKmers[j];
      // because i want assurence that there will be no duplicate nodes for this test.
      // this helps to create better tests
      assert(!(kmrKmer1 == kmrKmer2));
    }
  }

  BloomFilter bfFilter = BloomFilter(3);
  bfFilter.AddVector(vKmers);
  CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
  KmerTraverser ktTraverser(bfFilter, cfpSet);
  SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
  BFSGraphTraversal bfsTraversal;
  std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);
  std::string strString = ktTraverser.PathToString(aNodesPath);
  assert(CompareAllCombosSequences(strString, "TGTTAGCAGTAAAAAAAAAAAAAAAGAAAAAAAAAATGTAGT"));
  //PrintAllFourVersions(strString);
}

void TestGraphWithCycleTraverseWithSimpleNode() {
  {
    std::string strSequence("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"); // 32 * A
    ReadData rdData("", strSequence, "", "");

    std::vector<Kmer> vKmers;
    for (Kmer kmrKmer : rdData) {
      vKmers.push_back(kmrKmer);
    }

    BloomFilter bfFilter = BloomFilter(3);
    bfFilter.AddVector(vKmers);
    CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
    KmerTraverser ktTraverser(bfFilter, cfpSet);
    SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
    BFSGraphTraversal bfsTraversal;
    std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);

    std::string strString = ktTraverser.PathToString(aNodesPath);
    assert(CompareAllCombosSequences(strString, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
    //PrintAllFourVersions(strString);
  }

}


void TestGraphWithCycleTraverseWithComplexNodes() {
  {
    std::string strSequence("TTAAAAAAAAAAAAAAAAAAAAAAAAAATTTTT");
    ReadData rdData("", strSequence, "", "");

    std::vector<Kmer> vKmers;
    for (Kmer kmrKmer : rdData) {
      vKmers.push_back(kmrKmer);
    }

    for (int i = 0; i < vKmers.size(); i++) {
      for (int j = i + 1; j < vKmers.size(); j++) {
        Kmer& kmrKmer1 = vKmers[i];
        Kmer& kmrKmer2 = vKmers[j];

        assert(!(kmrKmer1 == kmrKmer2));
      }
    }

    BloomFilter bfFilter = BloomFilter(3);
    bfFilter.AddVector(vKmers);
    CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
    KmerTraverser ktTraverser(bfFilter, cfpSet);
    SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
    BFSGraphTraversal bfsTraversal;
    std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);

    std::string strString = ktTraverser.PathToString(aNodesPath);
    assert(CompareAllCombosSequences(strString, "TTAAAAAAAAAAAAAAAAAAAAAAAAAATTTTT"));
    //PrintAllFourVersions(strString);
  }


  {
    std::vector<Kmer> vKmers;
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAT"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAATG"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAAAATGA"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAAATGAC"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAATGACG"));

    //vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAATGACG"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAATGACGA"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAATGACGAT"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAATGACGATT"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAATGACGATTG"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAATGACGATTGT"));

    //vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAAATGACG"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAAATGACGT"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAAATGACGTG"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAAATGACGTGT"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAAATGACGTGTA"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAAATGACGTGTAG"));
    vKmers.push_back(Kmer("AAAAAAAAAAAAAAAAAAAATGACGTGTAGT"));

    for (int i = 0; i < vKmers.size(); i++) {
      for (int j = i + 1; j < vKmers.size(); j++) {
        Kmer& kmrKmer1 = vKmers[i];
        Kmer& kmrKmer2 = vKmers[j];

        assert(!(kmrKmer1 == kmrKmer2));
      }
    }

    BloomFilter bfFilter = BloomFilter(3);
    bfFilter.AddVector(vKmers);
    CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
    KmerTraverser ktTraverser(bfFilter, cfpSet);
    SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
    BFSGraphTraversal bfsTraversal;
    std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);

    std::string strString = ktTraverser.PathToString(aNodesPath);
    assert(CompareAllCombosSequences(strString, "TGTTAGCAGTAAAAAAAAAAAAAAAAAAAAAAAAAATGTAGT"));
    //PrintAllFourVersions(strString);
  }
}

void TestGraphWithCycleTraverseWithComplexGraphStructure() {

  {
    std::string strAnker1Normal = "GGGCGCGCGCGCGCGCGCGCGCGCGCGCGCC";
    std::string strAnker1Inverz = "CCCGCGCGCGCGCGCGCGCGCGCGCGCGCGG";
    std::string strAnker2Normal = "TTTTTTTTTTAAAAAAAATTTTTTTTAAAAT";
    std::string strAnker2Inverz = "AAAAAAAAAATTTTTTTTAAAAAAAATTTTA";
    std::string strAnker3Normal = "GAAAAAGGCGGGGGGGAAAAAAAAAGGGGGC";
    std::string strAnker3Inverz = "CTTTTTCCGCCCCCCCTTTTTTTTTCCCCCG";

    std::string strSeq1 = "TT" + strAnker1Normal + "AAA" + strAnker2Normal + "C" + strAnker3Normal + "TAAATCGTAGTTTATAG";
    std::string strSeq2 = "AATT" + strAnker1Inverz + "A" + strAnker2Inverz + "CTGA" + strAnker3Inverz + "TA";

    std::string strResult = "AATT" + strAnker1Inverz + "TTT" + strAnker2Inverz + "CTGA" + strAnker3Inverz + "ATTTAGCATCAAATATC";

    std::vector<std::string> vSequences = {
      strSeq1,
      strSeq2
    };

    std::vector<Kmer> vKmers = Factory(vSequences);

    BloomFilter bfFilter = BloomFilter(3);
    bfFilter.AddVector(vKmers);
    CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
    KmerTraverser ktTraverser(bfFilter, cfpSet);
    SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
    BFSGraphTraversal bfsTraversal;
    std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);

    std::string strString = ktTraverser.PathToString(aNodesPath);
    //PrintAllFourVersions(strString);
    //PrintAllFourVersions(strResult);
    //assert(CompareAllCombosSequences(strString, strResult));
    //ktTraverser.PathToString(aNodesPath);
  }

  {
    // GTAGCTTAGTTAAGGGGATGTATAAAGCTAG
    std::vector<std::string> vSeq = { "GTAGCTTAGTTAAGGGGATGTATAAAGCTAGGTAGCTTAGTTAAGGGGATGTATAAAGCTA" };
    std::vector<Kmer> vKmers = Factory(vSeq);

    for (int i = 0; i < vKmers.size(); i++) {
      for (int j = i + 1; j < vKmers.size(); j++) {
        Kmer& kmrKmer1 = vKmers[i];
        Kmer& kmrKmer2 = vKmers[j];

        assert(!(kmrKmer1 == kmrKmer2));
      }
    }

    BloomFilter bfFilter = BloomFilter(3);
    bfFilter.AddVector(vKmers);
    CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
    KmerTraverser ktTraverser(bfFilter, cfpSet);
    SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
    BFSGraphTraversal bfsTraversal;
    std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);

    std::string strString = ktTraverser.PathToString(aNodesPath);
    assert(CompareAllCombosSequences(strString, "GTAGCTTAGTTAAGGGGATGTATAAAGCTAGGTAGCTTAGTTAAGGGGATGTATAAAGCTA"));
    //PrintAllFourVersions(strString);

  }

  {
    // GTAGCTTAGTTAAGGGGATGTATAAAGCTAG
    std::vector<std::string> vSeq = { "CGTAGCTTAGTTAAGGGGATGTATAAAGCTAGGTAGCTTAGTTAAGGGGATGTATAAAGCTA" };
    std::vector<Kmer> vKmers = Factory(vSeq);

    for (int i = 0; i < vKmers.size(); i++) {
      for (int j = i + 1; j < vKmers.size(); j++) {
        Kmer& kmrKmer1 = vKmers[i];
        Kmer& kmrKmer2 = vKmers[j];

        assert(!(kmrKmer1 == kmrKmer2));
      }
    }

    BloomFilter bfFilter = BloomFilter(3);
    bfFilter.AddVector(vKmers);
    CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
    KmerTraverser ktTraverser(bfFilter, cfpSet);
    SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
    BFSGraphTraversal bfsTraversal;
    std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);

    std::string strString = ktTraverser.PathToString(aNodesPath);
    assert(CompareAllCombosSequences(strString, "CGTAGCTTAGTTAAGGGGATGTATAAAGCTAGGTAGCTTAGTTAAGGGGATGTATAAAGCTA") ||
           CompareAllCombosSequences(strString, "CATCGAAATATGTAGGGGAATTGATTCGATGGATCGAAATATGTAGGGGAATTGATTCGATG"));
    //PrintAllFourVersions(strString);

  }

}

void TestNodePrint1() {
  std::vector<Kmer> vKmers;
  vKmers.push_back(Kmer("AGTTTTTGTTTTTTCTTTTTTTTTTTTTTTT")); // 1
  vKmers.push_back(Kmer("GTTTTTGTTTTTTCTTTTTTTTTTTTTTTTT")); // 2
  vKmers.push_back(Kmer("TTTTTGTTTTTTCTTTTTTTTTTTTTTTTTT")); // 3
  vKmers.push_back(Kmer("TTTTGTTTTTTCTTTTTTTTTTTTTTTTTTC")); // 4
  vKmers.push_back(Kmer("TTTGTTTTTTCTTTTTTTTTTTTTTTTTTCG")); // 5
  //vKmers.push_back(Kmer("AAAACAAAAAAGAAAAAAAAAAAAAAAAAAG")); // 6 ( 4 inverted)
  vKmers.push_back(Kmer("AAACAAAAAAGAAAAAAAAAAAAAAAAAAGT")); // 7
  vKmers.push_back(Kmer("AACAAAAAAGAAAAAAAAAAAAAAAAAAGTA")); // 8

  BloomFilter bfFilter = BloomFilter(3);
  bfFilter.AddVector(vKmers);
  CriticalFalsePositiveSet cfpSet = CriticalFalsePositiveSet(vKmers, bfFilter, 5);
  KmerTraverser ktTraverser(bfFilter, cfpSet);
  SpaceEfficientMultiGraphBasedOnBloomFilter mgrMultiGraph(vKmers[0], ktTraverser);
  BFSGraphTraversal bfsTraversal;
  std::vector<Node*> aNodesPath = bfsTraversal.TraverseGraph(mgrMultiGraph);
  /*for (Node* no : aNodesPath) {
    no->PrintNode();
  }*/
  std::string strString = ktTraverser.PathToString(aNodesPath);
  assert(CompareAllCombosSequences(strString, "AGTTTTTGTTTTTTCTTTTTTTTTTTTTTTTTTCAT"));
  //PrintAllFourVersions(strString);
}


void TestGraph() {
  TestGraphWithCycleTraverseWithComplexGraphStructure();
   TestNodePrint1();
  TestGraphWithoutCycleTraverseWithSimpleNodes();
  TestGraphWithoutCycleTraverseWithComplexNodes();
  TestGraphWithoutCycleSimpleInvertedOverlap();

  TestGraphWithCycleTraverseWithSimpleNode();
  TestGraphWithCycleTraverseWithComplexNodes();
}

void Test()
{
  TestKmer();
  TestNextKmer();
  TheoryTest();
  TestGraph();
}
